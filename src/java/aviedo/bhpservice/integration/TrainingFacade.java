/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.integration;

import aviedo.bhpservice.entities.Training;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marcin
 */
@Stateless
public class TrainingFacade extends AbstractFacade<Training> {

    @PersistenceContext(unitName = "BHPServicePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrainingFacade() {
        super(Training.class);
    }
    
    public Training update(Training entity) {
        Training en = getEntityManager().merge(entity);
        getEntityManager().flush();
        return en;
    }
}
