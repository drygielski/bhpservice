/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.integration;

import aviedo.bhpservice.entities.Company;
import aviedo.bhpservice.entities.Employee;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marcin
 */
@Stateless
public class EmployeeFacade extends AbstractFacade<Employee> {

    @PersistenceContext(unitName = "BHPServicePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }
    
    public List<Employee> findCompanyID(Company com) {
        try {
            return em.createNamedQuery("Employee.findByEmployeeCompanyID").setParameter("employeeCompanyID", com).getResultList();
        }catch(NoResultException e) {
            System.out.println("Facade findCompanyID exception: " + e.toString());
            return null;
        }
    }
    
}
