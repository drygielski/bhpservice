/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.integration;

import aviedo.bhpservice.entities.Incidentcausereference;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marcin
 */
@Stateless
public class IncidentcausereferenceFacade extends AbstractFacade<Incidentcausereference> {

    @PersistenceContext(unitName = "BHPServicePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IncidentcausereferenceFacade() {
        super(Incidentcausereference.class);
    }
    
}
