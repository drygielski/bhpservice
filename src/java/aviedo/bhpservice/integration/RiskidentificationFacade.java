/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.integration;

import aviedo.bhpservice.entities.Riskidentification;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marcin
 */
@Stateless
public class RiskidentificationFacade extends AbstractFacade<Riskidentification> {

    @PersistenceContext(unitName = "BHPServicePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RiskidentificationFacade() {
        super(Riskidentification.class);
    }
    
}
