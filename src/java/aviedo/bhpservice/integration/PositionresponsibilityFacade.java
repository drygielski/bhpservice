/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.integration;

import aviedo.bhpservice.entities.Positionresponsibility;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marcin
 */
@Stateless
public class PositionresponsibilityFacade extends AbstractFacade<Positionresponsibility> {

    @PersistenceContext(unitName = "BHPServicePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionresponsibilityFacade() {
        super(Positionresponsibility.class);
    }
    
}
