/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "usercredential")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usercredential.findAll", query = "SELECT u FROM Usercredential u")
    , @NamedQuery(name = "Usercredential.findByUserCredentialID", query = "SELECT u FROM Usercredential u WHERE u.userCredentialID = :userCredentialID")
    , @NamedQuery(name = "Usercredential.findByUserCredentialLogin", query = "SELECT u FROM Usercredential u WHERE u.userCredentialLogin = :userCredentialLogin")})
public class Usercredential implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userCredentialID")
    private Integer userCredentialID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "userCredentialLogin")
    private String userCredentialLogin;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "userCredentialPassword")
    private String userCredentialPassword;
    @Lob
    @Size(max = 65535)
    @Column(name = "userCredentialSchema")
    private String userCredentialSchema;

    public Usercredential() {
    }

    public Usercredential(Integer userCredentialID) {
        this.userCredentialID = userCredentialID;
    }

    public Usercredential(Integer userCredentialID, String userCredentialLogin, String userCredentialPassword) {
        this.userCredentialID = userCredentialID;
        this.userCredentialLogin = userCredentialLogin;
        this.userCredentialPassword = userCredentialPassword;
    }

    public Integer getUserCredentialID() {
        return userCredentialID;
    }

    public void setUserCredentialID(Integer userCredentialID) {
        this.userCredentialID = userCredentialID;
    }

    public String getUserCredentialLogin() {
        return userCredentialLogin;
    }

    public void setUserCredentialLogin(String userCredentialLogin) {
        this.userCredentialLogin = userCredentialLogin;
    }

    public String getUserCredentialPassword() {
        return userCredentialPassword;
    }

    public void setUserCredentialPassword(String userCredentialPassword) {
        this.userCredentialPassword = userCredentialPassword;
    }

    public String getUserCredentialSchema() {
        return userCredentialSchema;
    }

    public void setUserCredentialSchema(String userCredentialSchema) {
        this.userCredentialSchema = userCredentialSchema;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userCredentialID != null ? userCredentialID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usercredential)) {
            return false;
        }
        Usercredential other = (Usercredential) object;
        if ((this.userCredentialLogin == null && other.userCredentialLogin != null) || (this.userCredentialLogin != null && !this.userCredentialLogin.equals(other.userCredentialLogin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.security.entities.Usercredential[ userCredentialID=" + userCredentialLogin + " ]";
    }
}
