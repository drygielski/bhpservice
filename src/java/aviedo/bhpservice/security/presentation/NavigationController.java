/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.security.presentation;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Marcin
 */
@Named("navigationController")
@SessionScoped
public class NavigationController implements Serializable {
    public String redirectToLogin() {
        return "/login.xhtml?faces-redirect=true";
    }
    public String toLogin() {
        return "/login.xhtml";
    }
    public String redirectToEmployee() {
        return "/userContent/employee/List.xhtml?faces-redirect=true";
    }
    public String toEmployee() {
        return "/userContent/employee/List.xhtml.xhtml";
    }
    public String redirectToCompany() {
        return "/userContent/company/List.xhtml?faces-redirect=true";
    }
    public String toCompany() {
        return "/userContent/company/List.xhtml.xhtml";
    }
    
    public String toTrainingGroup() {
        return "/userContent/traininggroup/List.xhtml";
    }
    public String redirectToTrainingGroup() {
        return "/userContent/traininggroup/List.xhtml?faces-redirect=true";
    }
    public String redirectToPosition() {
        return "/userContent/position/List.xhtml?faces-redirect=true";
    }
    public String redirectToRiskAnalisis() {
        return "/userContent/riskanalysis/List.xhtml?faces-redirect=true";
    }
    public String redirectToIncident() {
        return "/userContent/incident/List.xhtml?faces-redirect=true";
    }
    public String redirectToUser() {
        return "/userContent/user/List.xhtml?faces-redirect=true";
    }
    public String redirectToClient() {
        return "/userContent/client/List.xhtml?faces-redirect=true";
    }
    
    public String redirectToWelcome() {
        return "/userContent/index.xhtml?faces-redirect=true";
    }
    public String toWelcome() {
        return "/userContent/index.xhtml";
    }
}