package aviedo.bhpservice.security.presentation;

import aviedo.bhpservice.entities.User;
import aviedo.bhpservice.presentation.UserController;
import aviedo.bhpservice.security.Usercredential;
import aviedo.bhpservice.security.presentation.util.JsfUtil;
import aviedo.bhpservice.security.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.security.integration.UsercredentialFacade;

import java.io.Serializable;
import java.security.MessageDigest;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletResponse;

@Named("usercredentialController")
@SessionScoped
public class UsercredentialController implements Serializable {

    @EJB
    private aviedo.bhpservice.security.integration.UsercredentialFacade ejbFacade;
    private List<Usercredential> items = null;
    private Usercredential selected;
    private String userLogin;
    private String userPassword;
    
    @Inject 
    private UserController user;
    
    private NavigationController navigationController = new NavigationController();


    public String getUserLogin() {
        return userLogin;
    }
    
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
    
    public UsercredentialController() {
    }

    public Usercredential getSelected() {
        return selected;
    }

    public void setSelected(Usercredential selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private UsercredentialFacade getFacade() {
        return ejbFacade;
    }

    public Usercredential prepareCreate() {
        selected = new Usercredential();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("UsercredentialCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("UsercredentialUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("UsercredentialDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Usercredential> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                selected.setUserCredentialPassword(getSaltedHashMD5(selected.getUserCredentialPassword()));
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Usercredential getUsercredential(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Usercredential> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Usercredential> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Usercredential.class)
    public static class UsercredentialControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsercredentialController controller = (UsercredentialController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usercredentialController");
            return controller.getUsercredential(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usercredential) {
                Usercredential o = (Usercredential) object;
                return getStringKey(o.getUserCredentialID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Usercredential.class.getName()});
                return null;
            }
        }

    }
    
    public String createUser() {
        JDBCValidation validator = new JDBCValidation();
        
        int createCode = validator.CreateUser(userLogin, userPassword);
        if(createCode == -2) {
            FacesMessage msg;
            msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("CreateUser_ErrorUserAlreadyExists"), "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return null;
        }
        if(createCode == -1) {
            FacesMessage msg;
            msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("CreateUserError"), "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return null;
        }
        return navigationController.redirectToLogin();
    } 

    public String doLogin() {
        System.out.println("Logowanie");
        
        //Connect with JDBC to get validate the user and get schema
        JDBCValidation validator = new JDBCValidation(userLogin, userPassword);
        
        if(validator.getUserSchema() != null) {
            System.out.println("Pierwsza faza walidacji - OK - " + validator.getUserSchema());
            User refUser = user.getUserByLogin(userLogin);
            user.setSelected(refUser);
            if(refUser != null) {
                System.out.println("Druga faza walidacji - OK - " + refUser.getUserLogin());
                if(refUser.getUserIsActive()) {
                    FacesContext.getCurrentInstance().getExternalContext()
                        .getSessionMap().put("userLogin", userLogin);
                    return navigationController.redirectToEmployee();
                }else {
                    // Validation failed
                    System.out.println("Uzytkownik nie aktywny");
                    FacesMessage msg;
                    msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("Validation_UserNotActive"), "ERROR MSG");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    return navigationController.toLogin();
                }
                
                        
            }else {
                // Validation failed
                System.out.println("Druga faza walidacji - FAIL");
                FacesMessage msg;
                msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("Validation_UserNotDefined"), "ERROR MSG");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return navigationController.toLogin();
            }
        }else{
            // Validation failed
            System.out.println("Pierwsza faza walidacji - FAIL");
            FacesMessage msg;
            msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("Validation_InvalidUserOrPassword"), "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return navigationController.toLogin();
        }
        
    }
    
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        System.out.println(userLogin + " is logging out!");
        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().remove("userLogin", userLogin);
        // Set logout message
        FacesMessage msg = new FacesMessage(ResourceBundle.getBundle("/Bundle").getString("Validation_SuccessfulLogout"), "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
         
        return navigationController.toLogin();
    }
    
    private String getSaltedHashMD5(String text) {
        try {
            text += "Res3463gseERwhjiuu";
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte[] bytes = md.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for(int i=0; i<bytes.length; i++) {
                stringBuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("Salted password: " + stringBuilder.toString());
            return stringBuilder.toString();
            }catch(Exception e) {
            System.out.println(e.toString());
            }
        return null;
    }
    
    
    private class JDBCValidation {
        
        // JDBC driver name and database URL   
        static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        static final String DB_URL = "jdbc:mysql://localhost:3306/bhpservicesecurityschema?zeroDateTimeBehavior=convertToNull" ;
        //  Database credentials
        static final String USER = "jdbcUser";
        static final String PASS = "haslo";
        
        private String userLogin;
        private String userPassword;
        private String userSchema;

        public String getUserLogin() {
            return userLogin;
        }
        public void setUserLogin(String userLogin) {
            this.userLogin = userLogin;
        }
        public String getUserPassword() {
            return userPassword;
        }
        public void setUserPassword(String userPassword) {
            this.userPassword = userPassword;
        }
        public String getUserSchema() {
            return userSchema;
        }
        public void setUserSchema(String userSchema) {
            this.userSchema = userSchema;
        }
        
        public JDBCValidation() { }
        
        public JDBCValidation(String login, String password) {
            userLogin = login;
            userPassword = password;
            Connection connection = null;
            PreparedStatement ps = null;
            
            try {                
                // Register JDBC driver
                Class.forName("com.mysql.jdbc.Driver");
                // Open a connection
                System.out.println("Connecting to database");
                connection = DriverManager.getConnection(DB_URL,USER,PASS);
                ps=connection.prepareStatement("select userCredentialSchema from bhpservicesecurityschema.usercredential where userCredentialLogin=? and userCredentialPassword=?");
                ps.setString(1, userLogin); 
                ps.setString(2, getSaltedHashMD5(userPassword));
                ResultSet rs=ps.executeQuery();
                if(rs.next()) {
                    userSchema = rs.getString("userCredentialSchema");
                    System.out.println("userSchema: " + userSchema);
                }
                rs.close();
                ps.close();
                connection.close();
            }catch(SQLException e) {
                System.out.println("SQL Exception: " + e.toString());
            }catch(Exception e) {
                System.out.println("Exception: " + e.toString());
            }finally {
                try {
                    if(ps != null) {
                        ps.close();
                    }
                }catch(SQLException e) {
                    // Nothing to do
                }
                try {
                    if(connection != null) {
                        connection.close();
                    }
                }catch(SQLException e) {
                    // Nothing to do
                }
            }
            System.out.println("End validation");
        }
        
        
        int CreateUser(String login, String password) {
            Connection connection = null;
            PreparedStatement ps = null;
            
            try {                
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(DB_URL,USER,PASS);
                ps=connection.prepareStatement("select userCredentialLogin from bhpservicesecurityschema.usercredential where userCredentialLogin=?");
                ps.setString(1, login); 
                ps.executeQuery();
                ResultSet rs=ps.executeQuery();
                if(rs.next()) {
                    return -2;
                }
                ps=connection.prepareStatement("insert into bhpservicesecurityschema.usercredential (userCredentialLogin, userCredentialPassword, userCredentialSchema) VALUES (?, ?, ?)");
                ps.setString(1, login); 
                ps.setString(2, getSaltedHashMD5(password));
                ps.setString(3, "default");
                ps.executeUpdate();
                ps.close();
                connection.close();
                System.out.println("Utworzono użytkownika");
                return 1;
            }catch(SQLException e) {
                System.out.println("SQL Exception: " + e.toString());
            }catch(Exception e) {
                System.out.println("Exception: " + e.toString());
            }finally {
                try {
                    if(ps != null) {
                        ps.close();
                    }
                }catch(SQLException e) {
                    // Nothing to do
                }
                try {
                    if(connection != null) {
                        connection.close();
                    }
                }catch(SQLException e) {
                    // Nothing to do
                }
            }
            
            return -1;
        }
    }
}