/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionqualificationreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionqualificationreference.findAll", query = "SELECT p FROM Positionqualificationreference p")
    , @NamedQuery(name = "Positionqualificationreference.findByPositionQualificationReferenceID", query = "SELECT p FROM Positionqualificationreference p WHERE p.positionQualificationReferenceID = :positionQualificationReferenceID")})
public class Positionqualificationreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionQualificationReferenceID")
    private Integer positionQualificationReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "positionQualificationReferenceName")
    private String positionQualificationReferenceName;
    @OneToMany(mappedBy = "positionQualificationPositionQualificationReferenceID")
    private Collection<Positionqualification> positionqualificationCollection;

    public Positionqualificationreference() {
    }

    public Positionqualificationreference(Integer positionQualificationReferenceID) {
        this.positionQualificationReferenceID = positionQualificationReferenceID;
    }

    public Positionqualificationreference(Integer positionQualificationReferenceID, String positionQualificationReferenceName) {
        this.positionQualificationReferenceID = positionQualificationReferenceID;
        this.positionQualificationReferenceName = positionQualificationReferenceName;
    }

    public Integer getPositionQualificationReferenceID() {
        return positionQualificationReferenceID;
    }

    public void setPositionQualificationReferenceID(Integer positionQualificationReferenceID) {
        this.positionQualificationReferenceID = positionQualificationReferenceID;
    }

    public String getPositionQualificationReferenceName() {
        return positionQualificationReferenceName;
    }

    public void setPositionQualificationReferenceName(String positionQualificationReferenceName) {
        this.positionQualificationReferenceName = positionQualificationReferenceName;
    }

    @XmlTransient
    public Collection<Positionqualification> getPositionqualificationCollection() {
        return positionqualificationCollection;
    }

    public void setPositionqualificationCollection(Collection<Positionqualification> positionqualificationCollection) {
        this.positionqualificationCollection = positionqualificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionQualificationReferenceID != null ? positionQualificationReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionqualificationreference)) {
            return false;
        }
        Positionqualificationreference other = (Positionqualificationreference) object;
        if ((this.positionQualificationReferenceID == null && other.positionQualificationReferenceID != null) || (this.positionQualificationReferenceID != null && !this.positionQualificationReferenceID.equals(other.positionQualificationReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return positionQualificationReferenceName;
    }
    
}
