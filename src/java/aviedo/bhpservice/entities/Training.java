/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "training")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Training.findAll", query = "SELECT t FROM Training t")
    , @NamedQuery(name = "Training.findByTrainingID", query = "SELECT t FROM "
            + "Training t WHERE t.trainingID = :trainingID")
    , @NamedQuery(name = "Training.findByTrainingRegisterNumber", query = "SELECT t "
            + "FROM Training t WHERE t.trainingRegisterNumber = :trainingRegisterNumber")})
public class Training implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trainingID")
    private Integer trainingID;
    @Column(name = "trainingRegisterNumber")
    private Short trainingRegisterNumber;
    @JoinColumn(name = "TrainingGroupTrainingGroupID", referencedColumnName = "trainingGroupID")
    @ManyToOne(optional = false)
    private Traininggroup trainingGroupTrainingGroupID;
    @JoinColumn(name = "trainingEmployeeID", referencedColumnName = "employeeID")
    @ManyToOne(optional = false)
    private Employee trainingEmployeeID;
    @OneToMany(mappedBy = "employeeInitialTrainingID")
    private Collection<Employee> employeeCollection;
    @OneToMany(mappedBy = "employeePeriodicTrainingID")
    private Collection<Employee> employeeCollection1;

    public Training() {
    }

    public Training(Integer trainingID) {
        this.trainingID = trainingID;
    }

    public Integer getTrainingID() {
        return trainingID;
    }

    public void setTrainingID(Integer trainingID) {
        this.trainingID = trainingID;
    }

    public Short getTrainingRegisterNumber() {
        return trainingRegisterNumber;
    }

    public void setTrainingRegisterNumber(Short trainingRegisterNumber) {
        this.trainingRegisterNumber = trainingRegisterNumber;
    }

    public Traininggroup getTrainingGroupTrainingGroupID() {
        return trainingGroupTrainingGroupID;
    }

    public void setTrainingGroupTrainingGroupID(Traininggroup trainingGroupTrainingGroupID) {
        this.trainingGroupTrainingGroupID = trainingGroupTrainingGroupID;
    }

    public Employee getTrainingEmployeeID() {
        return trainingEmployeeID;
    }

    public void setTrainingEmployeeID(Employee trainingEmployeeID) {
        this.trainingEmployeeID = trainingEmployeeID;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection1() {
        return employeeCollection1;
    }

    public void setEmployeeCollection1(Collection<Employee> employeeCollection1) {
        this.employeeCollection1 = employeeCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trainingID != null ? trainingID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Training)) {
            return false;
        }
        Training other = (Training) object;
        if ((this.trainingID == null && other.trainingID != null) || (this.trainingID != null && !this.trainingID.equals(other.trainingID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Training[ trainingID=" + trainingID + " ]";
    }
    
}
