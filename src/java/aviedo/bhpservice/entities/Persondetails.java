/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.text.SimpleDateFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "persondetails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persondetails.findAll", query = "SELECT p FROM Persondetails p")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsID", query = "SELECT p FROM Persondetails p WHERE p.personDetailsID = :personDetailsID")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsPersonID", query = "SELECT p FROM Persondetails p WHERE p.personDetailsPersonID = :personDetailsPersonID")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsEmail", query = "SELECT p FROM Persondetails p WHERE p.personDetailsEmail = :personDetailsEmail")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsPESEL", query = "SELECT p FROM Persondetails p WHERE p.personDetailsPESEL = :personDetailsPESEL")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsNIP", query = "SELECT p FROM Persondetails p WHERE p.personDetailsNIP = :personDetailsNIP")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsBirthDate", query = "SELECT p FROM Persondetails p WHERE p.personDetailsBirthDate = :personDetailsBirthDate")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsBirthPlace", query = "SELECT p FROM Persondetails p WHERE p.personDetailsBirthPlace = :personDetailsBirthPlace")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsMotherName", query = "SELECT p FROM Persondetails p WHERE p.personDetailsMotherName = :personDetailsMotherName")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsFatherName", query = "SELECT p FROM Persondetails p WHERE p.personDetailsFatherName = :personDetailsFatherName")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsDocumentTypeID", query = "SELECT p FROM Persondetails p WHERE p.personDetailsDocumentTypeID = :personDetailsDocumentTypeID")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsDocumentSeries", query = "SELECT p FROM Persondetails p WHERE p.personDetailsDocumentSeries = :personDetailsDocumentSeries")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsDocumentNumber", query = "SELECT p FROM Persondetails p WHERE p.personDetailsDocumentNumber = :personDetailsDocumentNumber")
    , @NamedQuery(name = "Persondetails.findByPersonDetailsLastUpdateTimestamp", query = "SELECT p FROM Persondetails p WHERE p.personDetailsLastUpdateTimestamp = :personDetailsLastUpdateTimestamp")})
public class Persondetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "personDetailsID")
    private Integer personDetailsID;
    @Size(max = 45)
    @Column(name = "personDetailsEmail")
    private String personDetailsEmail;
    @Size(max = 11)
    @Column(name = "personDetailsPESEL")
    private String personDetailsPESEL;
    @Size(max = 10)
    @Column(name = "personDetailsNIP")
    private String personDetailsNIP;
    @Column(name = "personDetailsBirthDate")
    @Temporal(TemporalType.DATE)
    private Date personDetailsBirthDate;
    @Size(max = 45)
    @Column(name = "personDetailsBirthPlace")
    private String personDetailsBirthPlace;
    @Size(max = 20)
    @Column(name = "personDetailsMotherName")
    private String personDetailsMotherName;
    @Size(max = 20)
    @Column(name = "personDetailsFatherName")
    private String personDetailsFatherName;
    @Size(max = 20)
    @Column(name = "personDetailsDocumentTypeID")
    private String personDetailsDocumentTypeID;
    @Size(max = 3)
    @Column(name = "personDetailsDocumentSeries")
    private String personDetailsDocumentSeries;
    @Column(name = "personDetailsDocumentNumber")
    private Integer personDetailsDocumentNumber;
    @Column(name = "personDetailsLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date personDetailsLastUpdateTimestamp;
    @JoinColumn(name = "personDetailsPersonID", referencedColumnName = "personID")
    @ManyToOne(optional = false)
    private Person personDetailsPersonID;

    public Persondetails() {
    }

    public Persondetails(Integer personDetailsID) {
        this.personDetailsID = personDetailsID;
    }

    public Integer getPersonDetailsID() {
        return personDetailsID;
    }

    public void setPersonDetailsID(Integer personDetailsID) {
        this.personDetailsID = personDetailsID;
    }

    public String getPersonDetailsEmail() {
        return personDetailsEmail;
    }

    public void setPersonDetailsEmail(String personDetailsEmail) {
        this.personDetailsEmail = personDetailsEmail;
    }

    public String getPersonDetailsPESEL() {
        return personDetailsPESEL;
    }

    public void setPersonDetailsPESEL(String personDetailsPESEL) {
        this.personDetailsPESEL = personDetailsPESEL;
    }

    public String getPersonDetailsNIP() {
        return personDetailsNIP;
    }

    public void setPersonDetailsNIP(String personDetailsNIP) {
        this.personDetailsNIP = personDetailsNIP;
    }

    public Date getPersonDetailsBirthDate() {
        return personDetailsBirthDate;
    }
    
    public String getPersonDetailsBirthDateDescription() {
        return Integer.toString(personDetailsBirthDate.getDay())+"/"+
               Integer.toString(personDetailsBirthDate.getMonth())+"/"+
               Integer.toString(personDetailsBirthDate.getYear());
    }

    public void setPersonDetailsBirthDate(Date personDetailsBirthDate) {
        this.personDetailsBirthDate = personDetailsBirthDate;
    }

    public String getPersonDetailsBirthPlace() {
        return personDetailsBirthPlace;
    }

    public void setPersonDetailsBirthPlace(String personDetailsBirthPlace) {
        this.personDetailsBirthPlace = personDetailsBirthPlace;
    }

    public String getPersonDetailsMotherName() {
        return personDetailsMotherName;
    }

    public void setPersonDetailsMotherName(String personDetailsMotherName) {
        this.personDetailsMotherName = personDetailsMotherName;
    }

    public String getPersonDetailsFatherName() {
        return personDetailsFatherName;
    }

    public void setPersonDetailsFatherName(String personDetailsFatherName) {
        this.personDetailsFatherName = personDetailsFatherName;
    }

    public String getPersonDetailsDocumentTypeID() {
        return personDetailsDocumentTypeID;
    }

    public void setPersonDetailsDocumentTypeID(String personDetailsDocumentTypeID) {
        this.personDetailsDocumentTypeID = personDetailsDocumentTypeID;
    }

    public String getPersonDetailsDocumentSeries() {
        return personDetailsDocumentSeries;
    }

    public void setPersonDetailsDocumentSeries(String personDetailsDocumentSeries) {
        this.personDetailsDocumentSeries = personDetailsDocumentSeries;
    }

    public Integer getPersonDetailsDocumentNumber() {
        return personDetailsDocumentNumber;
    }

    public void setPersonDetailsDocumentNumber(Integer personDetailsDocumentNumber) {
        this.personDetailsDocumentNumber = personDetailsDocumentNumber;
    }

    public Date getPersonDetailsLastUpdateTimestamp() {
        return personDetailsLastUpdateTimestamp;
    }

    public void setPersonDetailsLastUpdateTimestamp(Date personDetailsLastUpdateTimestamp) {
        this.personDetailsLastUpdateTimestamp = personDetailsLastUpdateTimestamp;
    }

    public Person getPersonDetailsPersonID() {
        return personDetailsPersonID;
    }

    public void setPersonDetailsPersonID(Person personDetailsPersonID) {
        this.personDetailsPersonID = personDetailsPersonID;
    }

    @Override
    public int hashCode() {
        System.out.println("Person details HASHCODE");
        int hash = 0;
        hash += (personDetailsPersonID != null ? personDetailsPersonID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        System.out.println("Person details EQUALS");
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persondetails)) {
            return false;
        }
        Persondetails other = (Persondetails) object;
        if ((this.personDetailsPersonID == null && other.personDetailsPersonID != null) || (this.personDetailsPersonID != null && !this.personDetailsPersonID.equals(other.personDetailsPersonID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        System.out.println("Person details TOSTRING");
        return "aviedo.bhpservice.entities.Persondetails[ personDetailsID=" + personDetailsID + " ]";
    }
    
}
