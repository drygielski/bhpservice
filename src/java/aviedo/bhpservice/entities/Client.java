/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findByClientID", query = "SELECT c FROM Client c WHERE c.clientID = :clientID")
    , @NamedQuery(name = "Client.findByClientName", query = "SELECT c FROM Client c WHERE c.clientName = :clientName")
    , @NamedQuery(name = "Client.findByClientNIP", query = "SELECT c FROM Client c WHERE c.clientNIP = :clientNIP")
    , @NamedQuery(name = "Client.findByClientREGON", query = "SELECT c FROM Client c WHERE c.clientREGON = :clientREGON")
    , @NamedQuery(name = "Client.findByClientLogoPath", query = "SELECT c FROM Client c WHERE c.clientLogoPath = :clientLogoPath")
    , @NamedQuery(name = "Client.findByClientCreationTimestamp", query = "SELECT c FROM Client c WHERE c.clientCreationTimestamp = :clientCreationTimestamp")
    , @NamedQuery(name = "Client.findByClientLastUpdateTimestamp", query = "SELECT c FROM Client c WHERE c.clientLastUpdateTimestamp = :clientLastUpdateTimestamp")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "clientID")
    private Integer clientID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "clientName")
    private String clientName;
    @Size(max = 10)
    @Column(name = "clientNIP")
    private String clientNIP;
    @Size(max = 14)
    @Column(name = "clientREGON")
    private String clientREGON;
    @Size(max = 45)
    @Column(name = "clientLogoPath")
    private String clientLogoPath;
    @Column(name = "clientCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date clientCreationTimestamp;
    @Column(name = "clientLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date clientLastUpdateTimestamp;
    @JoinColumn(name = "clientAdminID", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User clientAdminID;
    @OneToMany(mappedBy = "userClientID")
    private Collection<User> userCollection;

    public Client() {
    }

    public Client(Integer clientID) {
        this.clientID = clientID;
    }

    public Client(Integer clientID, String clientName) {
        this.clientID = clientID;
        this.clientName = clientName;
    }

    public Integer getClientID() {
        return clientID;
    }

    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNIP() {
        return clientNIP;
    }

    public void setClientNIP(String clientNIP) {
        this.clientNIP = clientNIP;
    }

    public String getClientREGON() {
        return clientREGON;
    }

    public void setClientREGON(String clientREGON) {
        this.clientREGON = clientREGON;
    }

    public String getClientLogoPath() {
        return clientLogoPath;
    }

    public void setClientLogoPath(String clientLogoPath) {
        this.clientLogoPath = clientLogoPath;
    }

    public Date getClientCreationTimestamp() {
        return clientCreationTimestamp;
    }

    public void setClientCreationTimestamp(Date clientCreationTimestamp) {
        this.clientCreationTimestamp = clientCreationTimestamp;
    }

    public Date getClientLastUpdateTimestamp() {
        return clientLastUpdateTimestamp;
    }

    public void setClientLastUpdateTimestamp(Date clientLastUpdateTimestamp) {
        this.clientLastUpdateTimestamp = clientLastUpdateTimestamp;
    }

    public User getClientAdminID() {
        return clientAdminID;
    }

    public void setClientAdminID(User clientAdminID) {
        this.clientAdminID = clientAdminID;
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientID != null ? clientID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.clientID == null && other.clientID != null) || (this.clientID != null && !this.clientID.equals(other.clientID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Client[ clientID=" + clientID + " ]";
    }
    
}
