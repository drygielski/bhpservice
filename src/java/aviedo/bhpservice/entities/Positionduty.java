/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionduty")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionduty.findAll", query = "SELECT p FROM Positionduty p")
    , @NamedQuery(name = "Positionduty.findByPositionDutyID", query = "SELECT p FROM Positionduty p WHERE p.positionDutyID = :positionDutyID")})
public class Positionduty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionDutyID")
    private Integer positionDutyID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "positionDutyName")
    private String positionDutyName;
    @JoinColumn(name = "positionDutyPositionID", referencedColumnName = "positionID")
    @ManyToOne(optional = false)
    private Position positionDutyPositionID;

    public Positionduty() {
    }

    public Positionduty(Integer positionDutyID) {
        this.positionDutyID = positionDutyID;
    }

    public Positionduty(Integer positionDutyID, String positionDutyName) {
        this.positionDutyID = positionDutyID;
        this.positionDutyName = positionDutyName;
    }

    public Integer getPositionDutyID() {
        return positionDutyID;
    }

    public void setPositionDutyID(Integer positionDutyID) {
        this.positionDutyID = positionDutyID;
    }

    public String getPositionDutyName() {
        return positionDutyName;
    }

    public void setPositionDutyName(String positionDutyName) {
        this.positionDutyName = positionDutyName;
    }

    public Position getPositionDutyPositionID() {
        return positionDutyPositionID;
    }

    public void setPositionDutyPositionID(Position positionDutyPositionID) {
        this.positionDutyPositionID = positionDutyPositionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionDutyID != null ? positionDutyID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionduty)) {
            return false;
        }
        Positionduty other = (Positionduty) object;
        if ((this.positionDutyID == null && other.positionDutyID != null) || (this.positionDutyID != null && !this.positionDutyID.equals(other.positionDutyID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Positionduty[ positionDutyID=" + positionDutyID + " ]";
    }
    
}
