/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskprobabilityreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskprobabilityreference.findAll", query = "SELECT r FROM Riskprobabilityreference r")
    , @NamedQuery(name = "Riskprobabilityreference.findByRiskProbabilityReferenceID", query = "SELECT r FROM Riskprobabilityreference r WHERE r.riskProbabilityReferenceID = :riskProbabilityReferenceID")
    , @NamedQuery(name = "Riskprobabilityreference.findByRiskProbabilityReferenceName", query = "SELECT r FROM Riskprobabilityreference r WHERE r.riskProbabilityReferenceName = :riskProbabilityReferenceName")})
public class Riskprobabilityreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskProbabilityReferenceID")
    private Short riskProbabilityReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "riskProbabilityReferenceName")
    private String riskProbabilityReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskProbabilityReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;

    public Riskprobabilityreference() {
    }

    public Riskprobabilityreference(Short riskProbabilityReferenceID) {
        this.riskProbabilityReferenceID = riskProbabilityReferenceID;
    }

    public Riskprobabilityreference(Short riskProbabilityReferenceID, String riskProbabilityReferenceName) {
        this.riskProbabilityReferenceID = riskProbabilityReferenceID;
        this.riskProbabilityReferenceName = riskProbabilityReferenceName;
    }

    public Short getRiskProbabilityReferenceID() {
        return riskProbabilityReferenceID;
    }

    public void setRiskProbabilityReferenceID(Short riskProbabilityReferenceID) {
        this.riskProbabilityReferenceID = riskProbabilityReferenceID;
    }

    public String getRiskProbabilityReferenceName() {
        return riskProbabilityReferenceName;
    }

    public void setRiskProbabilityReferenceName(String riskProbabilityReferenceName) {
        this.riskProbabilityReferenceName = riskProbabilityReferenceName;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskProbabilityReferenceID != null ? riskProbabilityReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskprobabilityreference)) {
            return false;
        }
        Riskprobabilityreference other = (Riskprobabilityreference) object;
        if ((this.riskProbabilityReferenceID == null && other.riskProbabilityReferenceID != null) || (this.riskProbabilityReferenceID != null && !this.riskProbabilityReferenceID.equals(other.riskProbabilityReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskprobabilityreference[ riskProbabilityReferenceID=" + riskProbabilityReferenceID + " ]";
    }
    
}
