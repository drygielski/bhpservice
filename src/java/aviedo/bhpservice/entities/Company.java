/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "company")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Company.findAll", query = "SELECT c FROM Company c")
    , @NamedQuery(name = "Company.findByCompanyID", query = "SELECT c FROM Company c WHERE c.companyID = :companyID")
    , @NamedQuery(name = "Company.findByCompanyName", query = "SELECT c FROM Company c WHERE c.companyName = :companyName")
    , @NamedQuery(name = "Company.findByCompanyNIP", query = "SELECT c FROM Company c WHERE c.companyNIP = :companyNIP")
    , @NamedQuery(name = "Company.findByCompanyREGON", query = "SELECT c FROM Company c WHERE c.companyREGON = :companyREGON")
    , @NamedQuery(name = "Company.findByCompanyPhoneNumber", query = "SELECT c FROM Company c WHERE c.companyPhoneNumber = :companyPhoneNumber")
    , @NamedQuery(name = "Company.findByCompanyEmail", query = "SELECT c FROM Company c WHERE c.companyEmail = :companyEmail")
    , @NamedQuery(name = "Company.findByCompanyPKDCode", query = "SELECT c FROM Company c WHERE c.companyPKDCode = :companyPKDCode")
    , @NamedQuery(name = "Company.findByCompanyCreationTimestamp", query = "SELECT c FROM Company c WHERE c.companyCreationTimestamp = :companyCreationTimestamp")
    , @NamedQuery(name = "Company.findByCompanyLastUpdateTimestamp", query = "SELECT c FROM Company c WHERE c.companyLastUpdateTimestamp = :companyLastUpdateTimestamp")})
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "companyID")
    private Integer companyID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "companyName")
    private String companyName;
    @Size(max = 10)
    @Column(name = "companyNIP")
    private String companyNIP;
    @Size(max = 14)
    @Column(name = "companyREGON")
    private String companyREGON;
    @Size(max = 12)
    @Column(name = "companyPhoneNumber")
    private String companyPhoneNumber;
    @Size(max = 45)
    @Column(name = "companyEmail")
    private String companyEmail;
    @Size(max = 5)
    @Column(name = "companyPKDCode")
    private String companyPKDCode;
    @Column(name = "companyCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date companyCreationTimestamp;
    @Column(name = "companyLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date companyLastUpdateTimestamp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeCompanyID")
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "companyAddressID", referencedColumnName = "addressID")
    @ManyToOne(cascade = CascadeType.ALL)
    private Address companyAddressID;
    @JoinColumn(name = "companyOwnerID", referencedColumnName = "employeeID")
    @ManyToOne
    private Employee companyOwnerID;
    @JoinColumn(name = "companyRepresentativeID", referencedColumnName = "employeeID")
    @ManyToOne
    private Employee companyRepresentativeID;

    public Company() {
    }

    public Company(Integer companyID) {
        this.companyID = companyID;
    }

    public Company(Integer companyID, String companyName) {
        this.companyID = companyID;
        this.companyName = companyName;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNIP() {
        return companyNIP;
    }

    public void setCompanyNIP(String companyNIP) {
        this.companyNIP = companyNIP;
    }

    public String getCompanyREGON() {
        return companyREGON;
    }

    public void setCompanyREGON(String companyREGON) {
        this.companyREGON = companyREGON;
    }

    public String getCompanyPhoneNumber() {
        return companyPhoneNumber;
    }

    public void setCompanyPhoneNumber(String companyPhoneNumber) {
        this.companyPhoneNumber = companyPhoneNumber;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyPKDCode() {
        return companyPKDCode;
    }

    public void setCompanyPKDCode(String companyPKDCode) {
        this.companyPKDCode = companyPKDCode;
    }

    public Date getCompanyCreationTimestamp() {
        return companyCreationTimestamp;
    }

    public void setCompanyCreationTimestamp(Date companyCreationTimestamp) {
        this.companyCreationTimestamp = companyCreationTimestamp;
    }

    public Date getCompanyLastUpdateTimestamp() {
        return companyLastUpdateTimestamp;
    }

    public void setCompanyLastUpdateTimestamp(Date companyLastUpdateTimestamp) {
        this.companyLastUpdateTimestamp = companyLastUpdateTimestamp;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public Address getCompanyAddressID() {
        return companyAddressID;
    }

    public void setCompanyAddressID(Address companyAddressID) {
        this.companyAddressID = companyAddressID;
    }

    public Employee getCompanyOwnerID() {
        return companyOwnerID;
    }

    public void setCompanyOwnerID(Employee companyOwnerID) {
        this.companyOwnerID = companyOwnerID;
    }

    public Employee getCompanyRepresentativeID() {
        return companyRepresentativeID;
    }

    public void setCompanyRepresentativeID(Employee companyRepresentativeID) {
        this.companyRepresentativeID = companyRepresentativeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (companyID != null ? companyID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Company)) {
            return false;
        }
        Company other = (Company) object;
        if ((this.companyID == null && other.companyID != null) || (this.companyID != null && !this.companyID.equals(other.companyID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Company[ companyID=" + companyID + " ]";
    }
    
}
