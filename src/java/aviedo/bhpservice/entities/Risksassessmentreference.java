/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "risksassessmentreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Risksassessmentreference.findAll", query = "SELECT r FROM Risksassessmentreference r")
    , @NamedQuery(name = "Risksassessmentreference.findByRisksAssessmentReferenceID", query = "SELECT r FROM Risksassessmentreference r WHERE r.risksAssessmentReferenceID = :risksAssessmentReferenceID")
    , @NamedQuery(name = "Risksassessmentreference.findByRisksAssessmentReferenceName", query = "SELECT r FROM Risksassessmentreference r WHERE r.risksAssessmentReferenceName = :risksAssessmentReferenceName")})
public class Risksassessmentreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "risksAssessmentReferenceID")
    private Short risksAssessmentReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "risksAssessmentReferenceName")
    private String risksAssessmentReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRisksAssessmentReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;

    public Risksassessmentreference() {
    }

    public Risksassessmentreference(Short risksAssessmentReferenceID) {
        this.risksAssessmentReferenceID = risksAssessmentReferenceID;
    }

    public Risksassessmentreference(Short risksAssessmentReferenceID, String risksAssessmentReferenceName) {
        this.risksAssessmentReferenceID = risksAssessmentReferenceID;
        this.risksAssessmentReferenceName = risksAssessmentReferenceName;
    }

    public Short getRisksAssessmentReferenceID() {
        return risksAssessmentReferenceID;
    }

    public void setRisksAssessmentReferenceID(Short risksAssessmentReferenceID) {
        this.risksAssessmentReferenceID = risksAssessmentReferenceID;
    }

    public String getRisksAssessmentReferenceName() {
        return risksAssessmentReferenceName;
    }

    public void setRisksAssessmentReferenceName(String risksAssessmentReferenceName) {
        this.risksAssessmentReferenceName = risksAssessmentReferenceName;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (risksAssessmentReferenceID != null ? risksAssessmentReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Risksassessmentreference)) {
            return false;
        }
        Risksassessmentreference other = (Risksassessmentreference) object;
        if ((this.risksAssessmentReferenceID == null && other.risksAssessmentReferenceID != null) || (this.risksAssessmentReferenceID != null && !this.risksAssessmentReferenceID.equals(other.risksAssessmentReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Risksassessmentreference[ risksAssessmentReferenceID=" + risksAssessmentReferenceID + " ]";
    }
    
}
