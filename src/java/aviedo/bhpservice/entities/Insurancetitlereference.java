/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "insurancetitlereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insurancetitlereference.findAll", query = "SELECT i FROM Insurancetitlereference i")
    , @NamedQuery(name = "Insurancetitlereference.findByInsuranceTitleReferenceID", query = "SELECT i FROM Insurancetitlereference i WHERE i.insuranceTitleReferenceID = :insuranceTitleReferenceID")
    , @NamedQuery(name = "Insurancetitlereference.findByInsuranceTitleReferenceCode", query = "SELECT i FROM Insurancetitlereference i WHERE i.insuranceTitleReferenceCode = :insuranceTitleReferenceCode")})
public class Insurancetitlereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "insuranceTitleReferenceID")
    private Integer insuranceTitleReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "insuranceTitleReferenceCode")
    private String insuranceTitleReferenceCode;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "insuranceTitleReferenceDescription")
    private String insuranceTitleReferenceDescription;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insuranceTitleReferenceID")
    private Collection<Insurancetitle> insurancetitleCollection;

    public Insurancetitlereference() {
    }

    public Insurancetitlereference(Integer insuranceTitleReferenceID) {
        this.insuranceTitleReferenceID = insuranceTitleReferenceID;
    }

    public Insurancetitlereference(Integer insuranceTitleReferenceID, String insuranceTitleReferenceCode, String insuranceTitleReferenceDescription) {
        this.insuranceTitleReferenceID = insuranceTitleReferenceID;
        this.insuranceTitleReferenceCode = insuranceTitleReferenceCode;
        this.insuranceTitleReferenceDescription = insuranceTitleReferenceDescription;
    }

    public Integer getInsuranceTitleReferenceID() {
        return insuranceTitleReferenceID;
    }

    public void setInsuranceTitleReferenceID(Integer insuranceTitleReferenceID) {
        this.insuranceTitleReferenceID = insuranceTitleReferenceID;
    }

    public String getInsuranceTitleReferenceCode() {
        return insuranceTitleReferenceCode;
    }

    public void setInsuranceTitleReferenceCode(String insuranceTitleReferenceCode) {
        this.insuranceTitleReferenceCode = insuranceTitleReferenceCode;
    }

    public String getInsuranceTitleReferenceDescription() {
        return insuranceTitleReferenceDescription;
    }

    public void setInsuranceTitleReferenceDescription(String insuranceTitleReferenceDescription) {
        this.insuranceTitleReferenceDescription = insuranceTitleReferenceDescription;
    }

    @XmlTransient
    public Collection<Insurancetitle> getInsurancetitleCollection() {
        return insurancetitleCollection;
    }

    public void setInsurancetitleCollection(Collection<Insurancetitle> insurancetitleCollection) {
        this.insurancetitleCollection = insurancetitleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insuranceTitleReferenceID != null ? insuranceTitleReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insurancetitlereference)) {
            return false;
        }
        Insurancetitlereference other = (Insurancetitlereference) object;
        if ((this.insuranceTitleReferenceID == null && other.insuranceTitleReferenceID != null) || (this.insuranceTitleReferenceID != null && !this.insuranceTitleReferenceID.equals(other.insuranceTitleReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Insurancetitlereference[ insuranceTitleReferenceID=" + insuranceTitleReferenceID + " ]";
    }
    
}
