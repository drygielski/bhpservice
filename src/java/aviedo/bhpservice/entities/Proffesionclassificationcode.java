/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "proffesionclassificationcode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proffesionclassificationcode.findAll", query = "SELECT p FROM Proffesionclassificationcode p")
    , @NamedQuery(name = "Proffesionclassificationcode.findByProffesionClassificationCodeID", query = "SELECT p FROM Proffesionclassificationcode p WHERE p.proffesionClassificationCodeID = :proffesionClassificationCodeID")})
public class Proffesionclassificationcode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "proffesionClassificationCodeID")
    private Integer proffesionClassificationCodeID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "proffesionClassificationCodesName")
    private String proffesionClassificationCodesName;
    @OneToMany(mappedBy = "positionProffesionClassificationCodeID")
    private Collection<Position> positionCollection;

    public Proffesionclassificationcode() {
    }

    public Proffesionclassificationcode(Integer proffesionClassificationCodeID) {
        this.proffesionClassificationCodeID = proffesionClassificationCodeID;
    }

    public Proffesionclassificationcode(Integer proffesionClassificationCodeID, String proffesionClassificationCodesName) {
        this.proffesionClassificationCodeID = proffesionClassificationCodeID;
        this.proffesionClassificationCodesName = proffesionClassificationCodesName;
    }

    public Integer getProffesionClassificationCodeID() {
        return proffesionClassificationCodeID;
    }

    public void setProffesionClassificationCodeID(Integer proffesionClassificationCodeID) {
        this.proffesionClassificationCodeID = proffesionClassificationCodeID;
    }

    public String getProffesionClassificationCodesName() {
        return proffesionClassificationCodesName;
    }

    public void setProffesionClassificationCodesName(String proffesionClassificationCodesName) {
        this.proffesionClassificationCodesName = proffesionClassificationCodesName;
    }

    @XmlTransient
    public Collection<Position> getPositionCollection() {
        return positionCollection;
    }

    public void setPositionCollection(Collection<Position> positionCollection) {
        this.positionCollection = positionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proffesionClassificationCodeID != null ? proffesionClassificationCodeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proffesionclassificationcode)) {
            return false;
        }
        Proffesionclassificationcode other = (Proffesionclassificationcode) object;
        if ((this.proffesionClassificationCodeID == null && other.proffesionClassificationCodeID != null) || (this.proffesionClassificationCodeID != null && !this.proffesionClassificationCodeID.equals(other.proffesionClassificationCodeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Proffesionclassificationcode[ proffesionClassificationCodeID=" + proffesionClassificationCodeID + " ]";
    }
    
}
