/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskbasedocumentreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskbasedocumentreference.findAll", query = "SELECT r FROM Riskbasedocumentreference r")
    , @NamedQuery(name = "Riskbasedocumentreference.findByRiskBaseDocumentReferenceID", query = "SELECT r FROM Riskbasedocumentreference r WHERE r.riskBaseDocumentReferenceID = :riskBaseDocumentReferenceID")})
public class Riskbasedocumentreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskBaseDocumentReferenceID")
    private Integer riskBaseDocumentReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "riskBaseDocumentReferenceName")
    private String riskBaseDocumentReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskBaseDocumentRiskBaseDocumentReferenceID")
    private Collection<Riskbasedocument> riskbasedocumentCollection;

    public Riskbasedocumentreference() {
    }

    public Riskbasedocumentreference(Integer riskBaseDocumentReferenceID) {
        this.riskBaseDocumentReferenceID = riskBaseDocumentReferenceID;
    }

    public Riskbasedocumentreference(Integer riskBaseDocumentReferenceID, String riskBaseDocumentReferenceName) {
        this.riskBaseDocumentReferenceID = riskBaseDocumentReferenceID;
        this.riskBaseDocumentReferenceName = riskBaseDocumentReferenceName;
    }

    public Integer getRiskBaseDocumentReferenceID() {
        return riskBaseDocumentReferenceID;
    }

    public void setRiskBaseDocumentReferenceID(Integer riskBaseDocumentReferenceID) {
        this.riskBaseDocumentReferenceID = riskBaseDocumentReferenceID;
    }

    public String getRiskBaseDocumentReferenceName() {
        return riskBaseDocumentReferenceName;
    }

    public void setRiskBaseDocumentReferenceName(String riskBaseDocumentReferenceName) {
        this.riskBaseDocumentReferenceName = riskBaseDocumentReferenceName;
    }

    @XmlTransient
    public Collection<Riskbasedocument> getRiskbasedocumentCollection() {
        return riskbasedocumentCollection;
    }

    public void setRiskbasedocumentCollection(Collection<Riskbasedocument> riskbasedocumentCollection) {
        this.riskbasedocumentCollection = riskbasedocumentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskBaseDocumentReferenceID != null ? riskBaseDocumentReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskbasedocumentreference)) {
            return false;
        }
        Riskbasedocumentreference other = (Riskbasedocumentreference) object;
        if ((this.riskBaseDocumentReferenceID == null && other.riskBaseDocumentReferenceID != null) || (this.riskBaseDocumentReferenceID != null && !this.riskBaseDocumentReferenceID.equals(other.riskBaseDocumentReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskbasedocumentreference[ riskBaseDocumentReferenceID=" + riskBaseDocumentReferenceID + " ]";
    }
    
}
