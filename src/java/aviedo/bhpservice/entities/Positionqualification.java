/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionqualification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionqualification.findAll", query = "SELECT p FROM Positionqualification p")
    , @NamedQuery(name = "Positionqualification.findByPositionQualificationID", query = "SELECT p FROM Positionqualification p WHERE p.positionQualificationID = :positionQualificationID")})
public class Positionqualification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionQualificationID")
    private Integer positionQualificationID;
    @Lob
    @Size(max = 65535)
    @Column(name = "positionQualificationName")
    private String positionQualificationName;
    @JoinColumn(name = "positionQualificationPositionID", referencedColumnName = "positionID")
    @ManyToOne(optional = false)
    private Position positionQualificationPositionID;
    @JoinColumn(name = "positionQualificationPositionQualificationReferenceID", referencedColumnName = "positionQualificationReferenceID")
    @ManyToOne
    private Positionqualificationreference positionQualificationPositionQualificationReferenceID;

    public Positionqualification() {
    }

    public Positionqualification(Integer positionQualificationID) {
        this.positionQualificationID = positionQualificationID;
    }

    public Integer getPositionQualificationID() {
        return positionQualificationID;
    }

    public void setPositionQualificationID(Integer positionQualificationID) {
        this.positionQualificationID = positionQualificationID;
    }

    public String getPositionQualificationName() {
        return positionQualificationName;
    }

    public void setPositionQualificationName(String positionQualificationName) {
        this.positionQualificationName = positionQualificationName;
    }

    public Position getPositionQualificationPositionID() {
        return positionQualificationPositionID;
    }

    public void setPositionQualificationPositionID(Position positionQualificationPositionID) {
        this.positionQualificationPositionID = positionQualificationPositionID;
    }

    public Positionqualificationreference getPositionQualificationPositionQualificationReferenceID() {
        return positionQualificationPositionQualificationReferenceID;
    }

    public void setPositionQualificationPositionQualificationReferenceID(Positionqualificationreference positionQualificationPositionQualificationReferenceID) {
        this.positionQualificationPositionQualificationReferenceID = positionQualificationPositionQualificationReferenceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionQualificationID != null ? positionQualificationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionqualification)) {
            return false;
        }
        Positionqualification other = (Positionqualification) object;
        if ((this.positionQualificationID == null && other.positionQualificationID != null) || (this.positionQualificationID != null && !this.positionQualificationID.equals(other.positionQualificationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return positionQualificationName;
    }
    
}
