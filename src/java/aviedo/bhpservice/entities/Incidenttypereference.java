/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidenttypereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidenttypereference.findAll", query = "SELECT i FROM Incidenttypereference i")
    , @NamedQuery(name = "Incidenttypereference.findByIncidentTypeReferenceID", query = "SELECT i FROM Incidenttypereference i WHERE i.incidentTypeReferenceID = :incidentTypeReferenceID")
    , @NamedQuery(name = "Incidenttypereference.findByIncidentTypeReferenceName", query = "SELECT i FROM Incidenttypereference i WHERE i.incidentTypeReferenceName = :incidentTypeReferenceName")})
public class Incidenttypereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentTypeReferenceID")
    private Short incidentTypeReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "incidentTypeReferenceName")
    private String incidentTypeReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentTypeIncidentTypeReferenceID")
    private Collection<Incidenttype> incidenttypeCollection;

    public Incidenttypereference() {
    }

    public Incidenttypereference(Short incidentTypeReferenceID) {
        this.incidentTypeReferenceID = incidentTypeReferenceID;
    }

    public Incidenttypereference(Short incidentTypeReferenceID, String incidentTypeReferenceName) {
        this.incidentTypeReferenceID = incidentTypeReferenceID;
        this.incidentTypeReferenceName = incidentTypeReferenceName;
    }

    public Short getIncidentTypeReferenceID() {
        return incidentTypeReferenceID;
    }

    public void setIncidentTypeReferenceID(Short incidentTypeReferenceID) {
        this.incidentTypeReferenceID = incidentTypeReferenceID;
    }

    public String getIncidentTypeReferenceName() {
        return incidentTypeReferenceName;
    }

    public void setIncidentTypeReferenceName(String incidentTypeReferenceName) {
        this.incidentTypeReferenceName = incidentTypeReferenceName;
    }

    @XmlTransient
    public Collection<Incidenttype> getIncidenttypeCollection() {
        return incidenttypeCollection;
    }

    public void setIncidenttypeCollection(Collection<Incidenttype> incidenttypeCollection) {
        this.incidenttypeCollection = incidenttypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentTypeReferenceID != null ? incidentTypeReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidenttypereference)) {
            return false;
        }
        Incidenttypereference other = (Incidenttypereference) object;
        if ((this.incidentTypeReferenceID == null && other.incidentTypeReferenceID != null) || (this.incidentTypeReferenceID != null && !this.incidentTypeReferenceID.equals(other.incidentTypeReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidenttypereference[ incidentTypeReferenceID=" + incidentTypeReferenceID + " ]";
    }
    
}
