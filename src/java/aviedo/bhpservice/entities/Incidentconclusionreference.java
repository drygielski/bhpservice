/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentconclusionreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentconclusionreference.findAll", query = "SELECT i FROM Incidentconclusionreference i")
    , @NamedQuery(name = "Incidentconclusionreference.findByIncidentConclusionReferenceID", query = "SELECT i FROM Incidentconclusionreference i WHERE i.incidentConclusionReferenceID = :incidentConclusionReferenceID")})
public class Incidentconclusionreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentConclusionReferenceID")
    private Short incidentConclusionReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "incidentConclusionReferenceText")
    private String incidentConclusionReferenceText;
    @OneToMany(mappedBy = "incidentConclusionIncidentConclusionReferenceID")
    private Collection<Incidentconclusion> incidentconclusionCollection;

    public Incidentconclusionreference() {
    }

    public Incidentconclusionreference(Short incidentConclusionReferenceID) {
        this.incidentConclusionReferenceID = incidentConclusionReferenceID;
    }

    public Incidentconclusionreference(Short incidentConclusionReferenceID, String incidentConclusionReferenceText) {
        this.incidentConclusionReferenceID = incidentConclusionReferenceID;
        this.incidentConclusionReferenceText = incidentConclusionReferenceText;
    }

    public Short getIncidentConclusionReferenceID() {
        return incidentConclusionReferenceID;
    }

    public void setIncidentConclusionReferenceID(Short incidentConclusionReferenceID) {
        this.incidentConclusionReferenceID = incidentConclusionReferenceID;
    }

    public String getIncidentConclusionReferenceText() {
        return incidentConclusionReferenceText;
    }

    public void setIncidentConclusionReferenceText(String incidentConclusionReferenceText) {
        this.incidentConclusionReferenceText = incidentConclusionReferenceText;
    }

    @XmlTransient
    public Collection<Incidentconclusion> getIncidentconclusionCollection() {
        return incidentconclusionCollection;
    }

    public void setIncidentconclusionCollection(Collection<Incidentconclusion> incidentconclusionCollection) {
        this.incidentconclusionCollection = incidentconclusionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentConclusionReferenceID != null ? incidentConclusionReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentconclusionreference)) {
            return false;
        }
        Incidentconclusionreference other = (Incidentconclusionreference) object;
        if ((this.incidentConclusionReferenceID == null && other.incidentConclusionReferenceID != null) || (this.incidentConclusionReferenceID != null && !this.incidentConclusionReferenceID.equals(other.incidentConclusionReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentconclusionreference[ incidentConclusionReferenceID=" + incidentConclusionReferenceID + " ]";
    }
    
}
