/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incident")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incident.findAll", query = "SELECT i FROM Incident i")
    , @NamedQuery(name = "Incident.findByIncidentID", query = "SELECT i FROM Incident i WHERE i.incidentID = :incidentID")
    , @NamedQuery(name = "Incident.findByIncidentProtocolNumber", query = "SELECT i FROM Incident i WHERE i.incidentProtocolNumber = :incidentProtocolNumber")
    , @NamedQuery(name = "Incident.findByIncidentArrangementStartDate", query = "SELECT i FROM Incident i WHERE i.incidentArrangementStartDate = :incidentArrangementStartDate")
    , @NamedQuery(name = "Incident.findByIncidentArrangementEndDate", query = "SELECT i FROM Incident i WHERE i.incidentArrangementEndDate = :incidentArrangementEndDate")
    , @NamedQuery(name = "Incident.findByIncidentTime", query = "SELECT i FROM Incident i WHERE i.incidentTime = :incidentTime")
    , @NamedQuery(name = "Incident.findByIncidentDate", query = "SELECT i FROM Incident i WHERE i.incidentDate = :incidentDate")
    , @NamedQuery(name = "Incident.findByIncidentApplicationDate", query = "SELECT i FROM Incident i WHERE i.incidentApplicationDate = :incidentApplicationDate")
    , @NamedQuery(name = "Incident.findByIncidentAtWork", query = "SELECT i FROM Incident i WHERE i.incidentAtWork = :incidentAtWork")
    , @NamedQuery(name = "Incident.findByIncidentDocumentCreationDate", query = "SELECT i FROM Incident i WHERE i.incidentDocumentCreationDate = :incidentDocumentCreationDate")
    , @NamedQuery(name = "Incident.findByIncidentDocumentAcquaintDate", query = "SELECT i FROM Incident i WHERE i.incidentDocumentAcquaintDate = :incidentDocumentAcquaintDate")
    , @NamedQuery(name = "Incident.findByIncidentDocumentApprovalDate", query = "SELECT i FROM Incident i WHERE i.incidentDocumentApprovalDate = :incidentDocumentApprovalDate")
    , @NamedQuery(name = "Incident.findByIncidentDocumentReceiptDate", query = "SELECT i FROM Incident i WHERE i.incidentDocumentReceiptDate = :incidentDocumentReceiptDate")
    , @NamedQuery(name = "Incident.findByIncidentDocumentPostOrDeliveryDate", query = "SELECT i FROM Incident i WHERE i.incidentDocumentPostOrDeliveryDate = :incidentDocumentPostOrDeliveryDate")
    , @NamedQuery(name = "Incident.findByIncidentPostTrackingNumber", query = "SELECT i FROM Incident i WHERE i.incidentPostTrackingNumber = :incidentPostTrackingNumber")
    , @NamedQuery(name = "Incident.findByIncidentInsuranceType", query = "SELECT i FROM Incident i WHERE i.incidentInsuranceType = :incidentInsuranceType")})
public class Incident implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentID")
    private Integer incidentID;
    @Column(name = "incidentProtocolNumber")
    private Integer incidentProtocolNumber;
    @Column(name = "incidentArrangementStartDate")
    @Temporal(TemporalType.DATE)
    private Date incidentArrangementStartDate;
    @Column(name = "incidentArrangementEndDate")
    @Temporal(TemporalType.DATE)
    private Date incidentArrangementEndDate;
    @Column(name = "incidentTime")
    @Temporal(TemporalType.TIME)
    private Date incidentTime;
    @Column(name = "incidentDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDate;
    @Column(name = "incidentApplicationDate")
    @Temporal(TemporalType.DATE)
    private Date incidentApplicationDate;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentCircumstances")
    private String incidentCircumstances;
    @Column(name = "incidentAtWork")
    private Boolean incidentAtWork;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentAtWorkReason")
    private String incidentAtWorkReason;
    @Column(name = "incidentDocumentCreationDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDocumentCreationDate;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentDocumentDifficulties")
    private String incidentDocumentDifficulties;
    @Column(name = "incidentDocumentAcquaintDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDocumentAcquaintDate;
    @Column(name = "incidentDocumentApprovalDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDocumentApprovalDate;
    @Column(name = "incidentDocumentReceiptDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDocumentReceiptDate;
    @Column(name = "incidentDocumentPostOrDeliveryDate")
    @Temporal(TemporalType.DATE)
    private Date incidentDocumentPostOrDeliveryDate;
    @Size(max = 20)
    @Column(name = "incidentPostTrackingNumber")
    private String incidentPostTrackingNumber;
    @Size(max = 45)
    @Column(name = "incidentInsuranceType")
    private String incidentInsuranceType;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentEmployerNonCompliance")
    private String incidentEmployerNonCompliance;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentEmployeeNonCompliance")
    private String incidentEmployeeNonCompliance;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentEmployeeInsoberiety")
    private String incidentEmployeeInsoberiety;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentAttachmentIncidentID")
    private Collection<Incidentattachment> incidentattachmentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insuranceTitleIncidentID")
    private Collection<Insurancetitle> insurancetitleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentCauseIncidentID")
    private Collection<Incidentcause> incidentcauseCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentConclusionIncidentID")
    private Collection<Incidentconclusion> incidentconclusionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "witnessTestimonyIncidentID")
    private Collection<Witnesstestimony> witnesstestimonyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "victimTestimonyIncidentID")
    private Collection<Victimtestimony> victimtestimonyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentInjuryIncidentID")
    private Collection<Incidentinjury> incidentinjuryCollection;
    @JoinColumn(name = "incidentApplicantID", referencedColumnName = "employeeID")
    @ManyToOne
    private Employee incidentApplicantID;
    @JoinColumn(name = "incidentVictimID", referencedColumnName = "employeeID")
    @ManyToOne(optional = false)
    private Employee incidentVictimID;
    @JoinColumn(name = "incidentCompanyRepresentativeID", referencedColumnName = "employeeID")
    @ManyToOne
    private Employee incidentCompanyRepresentativeID;
    @JoinColumn(name = "incidentVictimFamilyMember", referencedColumnName = "personID")
    @ManyToOne
    private Person incidentVictimFamilyMember;
    @JoinColumn(name = "incidentOHSSpecialistID", referencedColumnName = "userID")
    @ManyToOne
    private User incidentOHSSpecialistID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentTypeIncidentID")
    private Collection<Incidenttype> incidenttypeCollection;

    public Incident() {
    }

    public Incident(Integer incidentID) {
        this.incidentID = incidentID;
    }

    public Integer getIncidentID() {
        return incidentID;
    }

    public void setIncidentID(Integer incidentID) {
        this.incidentID = incidentID;
    }

    public Integer getIncidentProtocolNumber() {
        return incidentProtocolNumber;
    }

    public void setIncidentProtocolNumber(Integer incidentProtocolNumber) {
        this.incidentProtocolNumber = incidentProtocolNumber;
    }

    public Date getIncidentArrangementStartDate() {
        return incidentArrangementStartDate;
    }

    public void setIncidentArrangementStartDate(Date incidentArrangementStartDate) {
        this.incidentArrangementStartDate = incidentArrangementStartDate;
    }

    public Date getIncidentArrangementEndDate() {
        return incidentArrangementEndDate;
    }

    public void setIncidentArrangementEndDate(Date incidentArrangementEndDate) {
        this.incidentArrangementEndDate = incidentArrangementEndDate;
    }

    public Date getIncidentTime() {
        return incidentTime;
    }

    public void setIncidentTime(Date incidentTime) {
        this.incidentTime = incidentTime;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public Date getIncidentApplicationDate() {
        return incidentApplicationDate;
    }

    public void setIncidentApplicationDate(Date incidentApplicationDate) {
        this.incidentApplicationDate = incidentApplicationDate;
    }

    public String getIncidentCircumstances() {
        return incidentCircumstances;
    }

    public void setIncidentCircumstances(String incidentCircumstances) {
        this.incidentCircumstances = incidentCircumstances;
    }

    public Boolean getIncidentAtWork() {
        return incidentAtWork;
    }

    public void setIncidentAtWork(Boolean incidentAtWork) {
        this.incidentAtWork = incidentAtWork;
    }

    public String getIncidentAtWorkReason() {
        return incidentAtWorkReason;
    }

    public void setIncidentAtWorkReason(String incidentAtWorkReason) {
        this.incidentAtWorkReason = incidentAtWorkReason;
    }

    public Date getIncidentDocumentCreationDate() {
        return incidentDocumentCreationDate;
    }

    public void setIncidentDocumentCreationDate(Date incidentDocumentCreationDate) {
        this.incidentDocumentCreationDate = incidentDocumentCreationDate;
    }

    public String getIncidentDocumentDifficulties() {
        return incidentDocumentDifficulties;
    }

    public void setIncidentDocumentDifficulties(String incidentDocumentDifficulties) {
        this.incidentDocumentDifficulties = incidentDocumentDifficulties;
    }

    public Date getIncidentDocumentAcquaintDate() {
        return incidentDocumentAcquaintDate;
    }

    public void setIncidentDocumentAcquaintDate(Date incidentDocumentAcquaintDate) {
        this.incidentDocumentAcquaintDate = incidentDocumentAcquaintDate;
    }

    public Date getIncidentDocumentApprovalDate() {
        return incidentDocumentApprovalDate;
    }

    public void setIncidentDocumentApprovalDate(Date incidentDocumentApprovalDate) {
        this.incidentDocumentApprovalDate = incidentDocumentApprovalDate;
    }

    public Date getIncidentDocumentReceiptDate() {
        return incidentDocumentReceiptDate;
    }

    public void setIncidentDocumentReceiptDate(Date incidentDocumentReceiptDate) {
        this.incidentDocumentReceiptDate = incidentDocumentReceiptDate;
    }

    public Date getIncidentDocumentPostOrDeliveryDate() {
        return incidentDocumentPostOrDeliveryDate;
    }

    public void setIncidentDocumentPostOrDeliveryDate(Date incidentDocumentPostOrDeliveryDate) {
        this.incidentDocumentPostOrDeliveryDate = incidentDocumentPostOrDeliveryDate;
    }

    public String getIncidentPostTrackingNumber() {
        return incidentPostTrackingNumber;
    }

    public void setIncidentPostTrackingNumber(String incidentPostTrackingNumber) {
        this.incidentPostTrackingNumber = incidentPostTrackingNumber;
    }

    public String getIncidentInsuranceType() {
        return incidentInsuranceType;
    }

    public void setIncidentInsuranceType(String incidentInsuranceType) {
        this.incidentInsuranceType = incidentInsuranceType;
    }

    public String getIncidentEmployerNonCompliance() {
        return incidentEmployerNonCompliance;
    }

    public void setIncidentEmployerNonCompliance(String incidentEmployerNonCompliance) {
        this.incidentEmployerNonCompliance = incidentEmployerNonCompliance;
    }

    public String getIncidentEmployeeNonCompliance() {
        return incidentEmployeeNonCompliance;
    }

    public void setIncidentEmployeeNonCompliance(String incidentEmployeeNonCompliance) {
        this.incidentEmployeeNonCompliance = incidentEmployeeNonCompliance;
    }

    public String getIncidentEmployeeInsoberiety() {
        return incidentEmployeeInsoberiety;
    }

    public void setIncidentEmployeeInsoberiety(String incidentEmployeeInsoberiety) {
        this.incidentEmployeeInsoberiety = incidentEmployeeInsoberiety;
    }

    @XmlTransient
    public Collection<Incidentattachment> getIncidentattachmentCollection() {
        return incidentattachmentCollection;
    }

    public void setIncidentattachmentCollection(Collection<Incidentattachment> incidentattachmentCollection) {
        this.incidentattachmentCollection = incidentattachmentCollection;
    }

    @XmlTransient
    public Collection<Insurancetitle> getInsurancetitleCollection() {
        return insurancetitleCollection;
    }

    public void setInsurancetitleCollection(Collection<Insurancetitle> insurancetitleCollection) {
        this.insurancetitleCollection = insurancetitleCollection;
    }

    @XmlTransient
    public Collection<Incidentcause> getIncidentcauseCollection() {
        return incidentcauseCollection;
    }

    public void setIncidentcauseCollection(Collection<Incidentcause> incidentcauseCollection) {
        this.incidentcauseCollection = incidentcauseCollection;
    }

    @XmlTransient
    public Collection<Incidentconclusion> getIncidentconclusionCollection() {
        return incidentconclusionCollection;
    }

    public void setIncidentconclusionCollection(Collection<Incidentconclusion> incidentconclusionCollection) {
        this.incidentconclusionCollection = incidentconclusionCollection;
    }

    @XmlTransient
    public Collection<Witnesstestimony> getWitnesstestimonyCollection() {
        return witnesstestimonyCollection;
    }

    public void setWitnesstestimonyCollection(Collection<Witnesstestimony> witnesstestimonyCollection) {
        this.witnesstestimonyCollection = witnesstestimonyCollection;
    }

    @XmlTransient
    public Collection<Victimtestimony> getVictimtestimonyCollection() {
        return victimtestimonyCollection;
    }

    public void setVictimtestimonyCollection(Collection<Victimtestimony> victimtestimonyCollection) {
        this.victimtestimonyCollection = victimtestimonyCollection;
    }

    @XmlTransient
    public Collection<Incidentinjury> getIncidentinjuryCollection() {
        return incidentinjuryCollection;
    }

    public void setIncidentinjuryCollection(Collection<Incidentinjury> incidentinjuryCollection) {
        this.incidentinjuryCollection = incidentinjuryCollection;
    }

    public Employee getIncidentApplicantID() {
        return incidentApplicantID;
    }

    public void setIncidentApplicantID(Employee incidentApplicantID) {
        this.incidentApplicantID = incidentApplicantID;
    }

    public Employee getIncidentVictimID() {
        return incidentVictimID;
    }

    public void setIncidentVictimID(Employee incidentVictimID) {
        this.incidentVictimID = incidentVictimID;
    }

    public Employee getIncidentCompanyRepresentativeID() {
        return incidentCompanyRepresentativeID;
    }

    public void setIncidentCompanyRepresentativeID(Employee incidentCompanyRepresentativeID) {
        this.incidentCompanyRepresentativeID = incidentCompanyRepresentativeID;
    }

    public Person getIncidentVictimFamilyMember() {
        return incidentVictimFamilyMember;
    }

    public void setIncidentVictimFamilyMember(Person incidentVictimFamilyMember) {
        this.incidentVictimFamilyMember = incidentVictimFamilyMember;
    }

    public User getIncidentOHSSpecialistID() {
        return incidentOHSSpecialistID;
    }

    public void setIncidentOHSSpecialistID(User incidentOHSSpecialistID) {
        this.incidentOHSSpecialistID = incidentOHSSpecialistID;
    }

    @XmlTransient
    public Collection<Incidenttype> getIncidenttypeCollection() {
        return incidenttypeCollection;
    }

    public void setIncidenttypeCollection(Collection<Incidenttype> incidenttypeCollection) {
        this.incidenttypeCollection = incidenttypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentID != null ? incidentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incident)) {
            return false;
        }
        Incident other = (Incident) object;
        if ((this.incidentID == null && other.incidentID != null) || (this.incidentID != null && !this.incidentID.equals(other.incidentID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incident[ incidentID=" + incidentID + " ]";
    }
    
}
