/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "person")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Person.findAll", query = "SELECT p FROM Person p")
    , @NamedQuery(name = "Person.findByPersonID", query = "SELECT p FROM Person p WHERE p.personID = :personID")
    , @NamedQuery(name = "Person.findByPersonFirstName", query = "SELECT p FROM Person p WHERE p.personFirstName = :personFirstName")
    , @NamedQuery(name = "Person.findByPersonSecondName", query = "SELECT p FROM Person p WHERE p.personSecondName = :personSecondName")
    , @NamedQuery(name = "Person.findByPersonLastName", query = "SELECT p FROM Person p WHERE p.personLastName = :personLastName")
    , @NamedQuery(name = "Person.findByPersonGender", query = "SELECT p FROM Person p WHERE p.personGender = :personGender")
    , @NamedQuery(name = "Person.findByPersonContactNumber", query = "SELECT p FROM Person p WHERE p.personContactNumber = :personContactNumber")
    , @NamedQuery(name = "Person.findByPersonCreationTimestamp", query = "SELECT p FROM Person p WHERE p.personCreationTimestamp = :personCreationTimestamp")
    , @NamedQuery(name = "Person.findByPersonLastUpdateTimestamp", query = "SELECT p FROM Person p WHERE p.personLastUpdateTimestamp = :personLastUpdateTimestamp")})
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "personID")
    private Integer personID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "personFirstName")
    private String personFirstName;
    @Size(max = 16)
    @Column(name = "personSecondName")
    private String personSecondName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "personLastName")
    private String personLastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "personGender")
    private String personGender;
    @Size(max = 12)
    @Column(name = "personContactNumber")
    private String personContactNumber;
    @Lob
    @Size(max = 65535)
    @Column(name = "personNotes")
    private String personNotes;
    @Column(name = "personCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date personCreationTimestamp;
    @Column(name = "personLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date personLastUpdateTimestamp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personDetailsPersonID")
    private Collection<Persondetails> persondetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeePersonID", orphanRemoval = true)
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "personAddressID", referencedColumnName = "addressID")
    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private Address personAddressID;
    @OneToMany(mappedBy = "incidentVictimFamilyMember")
    private Collection<Incident> incidentCollection;
    @OneToMany(mappedBy = "userPersonID", orphanRemoval = true)
    private Collection<User> userCollection;

    public Person() {
    }

    public Person(Integer personID) {
        this.personID = personID;
    }

    public Person(Integer personID, String personFirstName, String personLastName, String personGender) {
        this.personID = personID;
        this.personFirstName = personFirstName;
        this.personLastName = personLastName;
        this.personGender = personGender;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        System.out.println("Setting PersonFirstName: " + personFirstName);
        this.personFirstName = personFirstName;
    }

    public String getPersonSecondName() {
        return personSecondName;
    }

    public void setPersonSecondName(String personSecondName) {
        this.personSecondName = personSecondName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        System.out.println("Płeć " + personGender);
        if(personGender == "true") {
            this.personGender = "M";
        }
        if(personGender == "false") {
            this.personGender = "K";
        }
        this.personGender = personGender;
    }

    public String getPersonContactNumber() {
        return personContactNumber;
    }

    public void setPersonContactNumber(String personContactNumber) {
        this.personContactNumber = personContactNumber;
    }

    public String getPersonNotes() {
        return personNotes;
    }

    public void setPersonNotes(String personNotes) {
        System.out.println("Setting notes");
        this.personNotes = personNotes;
    }

    public Date getPersonCreationTimestamp() {
        return personCreationTimestamp;
    }

    public void setPersonCreationTimestamp(Date personCreationTimestamp) {
        this.personCreationTimestamp = personCreationTimestamp;
    }

    public Date getPersonLastUpdateTimestamp() {
        return personLastUpdateTimestamp;
    }

    public void setPersonLastUpdateTimestamp(Date personLastUpdateTimestamp) {
        this.personLastUpdateTimestamp = personLastUpdateTimestamp;
    }

    @XmlTransient
    public Collection<Persondetails> getPersondetailsCollection() {
        return persondetailsCollection;
    }

    public void setPersondetailsCollection(Collection<Persondetails> persondetailsCollection) {
        this.persondetailsCollection = persondetailsCollection;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public Address getPersonAddressID() {
        return personAddressID;
    }

    public void setPersonAddressID(Address personAddressID) {
        this.personAddressID = personAddressID;
    }

    @XmlTransient
    public Collection<Incident> getIncidentCollection() {
        return incidentCollection;
    }

    public void setIncidentCollection(Collection<Incident> incidentCollection) {
        this.incidentCollection = incidentCollection;
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personID != null ? personID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.personID == null && other.personID != null) || (this.personID != null && !this.personID.equals(other.personID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Person[ personID=" + personID + " ]";
    }
    public String getDescription() {
        if(personFirstName != null) {
            if(personLastName != null) {
                return personFirstName + " " + personLastName;
            }
            return personFirstName;
        }else {
            if(personLastName != null) {
                return personLastName;
            }
        }
        return "";
    }
    
}
