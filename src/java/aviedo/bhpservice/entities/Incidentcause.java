/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentcause")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentcause.findAll", query = "SELECT i FROM Incidentcause i")
    , @NamedQuery(name = "Incidentcause.findByIncidentCauseID", query = "SELECT i FROM Incidentcause i WHERE i.incidentCauseID = :incidentCauseID")})
public class Incidentcause implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentCauseID")
    private Integer incidentCauseID;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentCauseName")
    private String incidentCauseName;
    @JoinColumn(name = "incidentCauseIncidentCauseReferenceID", referencedColumnName = "incidentCauseReferenceID")
    @ManyToOne
    private Incidentcausereference incidentCauseIncidentCauseReferenceID;
    @JoinColumn(name = "incidentCauseIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident incidentCauseIncidentID;

    public Incidentcause() {
    }

    public Incidentcause(Integer incidentCauseID) {
        this.incidentCauseID = incidentCauseID;
    }

    public Integer getIncidentCauseID() {
        return incidentCauseID;
    }

    public void setIncidentCauseID(Integer incidentCauseID) {
        this.incidentCauseID = incidentCauseID;
    }

    public String getIncidentCauseName() {
        return incidentCauseName;
    }

    public void setIncidentCauseName(String incidentCauseName) {
        this.incidentCauseName = incidentCauseName;
    }

    public Incidentcausereference getIncidentCauseIncidentCauseReferenceID() {
        return incidentCauseIncidentCauseReferenceID;
    }

    public void setIncidentCauseIncidentCauseReferenceID(Incidentcausereference incidentCauseIncidentCauseReferenceID) {
        this.incidentCauseIncidentCauseReferenceID = incidentCauseIncidentCauseReferenceID;
    }

    public Incident getIncidentCauseIncidentID() {
        return incidentCauseIncidentID;
    }

    public void setIncidentCauseIncidentID(Incident incidentCauseIncidentID) {
        this.incidentCauseIncidentID = incidentCauseIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentCauseID != null ? incidentCauseID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentcause)) {
            return false;
        }
        Incidentcause other = (Incidentcause) object;
        if ((this.incidentCauseID == null && other.incidentCauseID != null) || (this.incidentCauseID != null && !this.incidentCauseID.equals(other.incidentCauseID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentcause[ incidentCauseID=" + incidentCauseID + " ]";
    }
    
}
