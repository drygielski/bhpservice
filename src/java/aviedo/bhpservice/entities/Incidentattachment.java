/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentattachment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentattachment.findAll", query = "SELECT i FROM Incidentattachment i")
    , @NamedQuery(name = "Incidentattachment.findByIncidentAttachmentID", query = "SELECT i FROM Incidentattachment i WHERE i.incidentAttachmentID = :incidentAttachmentID")
    , @NamedQuery(name = "Incidentattachment.findByIncidentAttachmentName", query = "SELECT i FROM Incidentattachment i WHERE i.incidentAttachmentName = :incidentAttachmentName")
    , @NamedQuery(name = "Incidentattachment.findByIncidentAttachmentPath", query = "SELECT i FROM Incidentattachment i WHERE i.incidentAttachmentPath = :incidentAttachmentPath")
    , @NamedQuery(name = "Incidentattachment.findByIncidentAttachmentType", query = "SELECT i FROM Incidentattachment i WHERE i.incidentAttachmentType = :incidentAttachmentType")})
public class Incidentattachment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentAttachmentID")
    private Integer incidentAttachmentID;
    @Size(max = 45)
    @Column(name = "incidentAttachmentName")
    private String incidentAttachmentName;
    @Size(max = 100)
    @Column(name = "incidentAttachmentPath")
    private String incidentAttachmentPath;
    @Size(max = 10)
    @Column(name = "incidentAttachmentType")
    private String incidentAttachmentType;
    @JoinColumn(name = "incidentAttachmentIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident incidentAttachmentIncidentID;

    public Incidentattachment() {
    }

    public Incidentattachment(Integer incidentAttachmentID) {
        this.incidentAttachmentID = incidentAttachmentID;
    }

    public Integer getIncidentAttachmentID() {
        return incidentAttachmentID;
    }

    public void setIncidentAttachmentID(Integer incidentAttachmentID) {
        this.incidentAttachmentID = incidentAttachmentID;
    }

    public String getIncidentAttachmentName() {
        return incidentAttachmentName;
    }

    public void setIncidentAttachmentName(String incidentAttachmentName) {
        this.incidentAttachmentName = incidentAttachmentName;
    }

    public String getIncidentAttachmentPath() {
        return incidentAttachmentPath;
    }

    public void setIncidentAttachmentPath(String incidentAttachmentPath) {
        this.incidentAttachmentPath = incidentAttachmentPath;
    }

    public String getIncidentAttachmentType() {
        return incidentAttachmentType;
    }

    public void setIncidentAttachmentType(String incidentAttachmentType) {
        this.incidentAttachmentType = incidentAttachmentType;
    }

    public Incident getIncidentAttachmentIncidentID() {
        return incidentAttachmentIncidentID;
    }

    public void setIncidentAttachmentIncidentID(Incident incidentAttachmentIncidentID) {
        this.incidentAttachmentIncidentID = incidentAttachmentIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentAttachmentID != null ? incidentAttachmentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentattachment)) {
            return false;
        }
        Incidentattachment other = (Incidentattachment) object;
        if ((this.incidentAttachmentID == null && other.incidentAttachmentID != null) || (this.incidentAttachmentID != null && !this.incidentAttachmentID.equals(other.incidentAttachmentID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentattachment[ incidentAttachmentID=" + incidentAttachmentID + " ]";
    }
    
}
