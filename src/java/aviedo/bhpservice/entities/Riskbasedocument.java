/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskbasedocument")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskbasedocument.findAll", query = "SELECT r FROM Riskbasedocument r")
    , @NamedQuery(name = "Riskbasedocument.findByRiskBaseDocumentID", query = "SELECT r FROM Riskbasedocument r WHERE r.riskBaseDocumentID = :riskBaseDocumentID")})
public class Riskbasedocument implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskBaseDocumentID")
    private Integer riskBaseDocumentID;
    @JoinColumn(name = "riskBaseDocumentRiskAnalysisID", referencedColumnName = "riskAnalysisID")
    @ManyToOne(optional = false)
    private Riskanalysis riskBaseDocumentRiskAnalysisID;
    @JoinColumn(name = "riskBaseDocumentRiskBaseDocumentReferenceID", referencedColumnName = "riskBaseDocumentReferenceID")
    @ManyToOne(optional = false)
    private Riskbasedocumentreference riskBaseDocumentRiskBaseDocumentReferenceID;

    public Riskbasedocument() {
    }

    public Riskbasedocument(Integer riskBaseDocumentID) {
        this.riskBaseDocumentID = riskBaseDocumentID;
    }

    public Integer getRiskBaseDocumentID() {
        return riskBaseDocumentID;
    }

    public void setRiskBaseDocumentID(Integer riskBaseDocumentID) {
        this.riskBaseDocumentID = riskBaseDocumentID;
    }

    public Riskanalysis getRiskBaseDocumentRiskAnalysisID() {
        return riskBaseDocumentRiskAnalysisID;
    }

    public void setRiskBaseDocumentRiskAnalysisID(Riskanalysis riskBaseDocumentRiskAnalysisID) {
        this.riskBaseDocumentRiskAnalysisID = riskBaseDocumentRiskAnalysisID;
    }

    public Riskbasedocumentreference getRiskBaseDocumentRiskBaseDocumentReferenceID() {
        return riskBaseDocumentRiskBaseDocumentReferenceID;
    }

    public void setRiskBaseDocumentRiskBaseDocumentReferenceID(Riskbasedocumentreference riskBaseDocumentRiskBaseDocumentReferenceID) {
        this.riskBaseDocumentRiskBaseDocumentReferenceID = riskBaseDocumentRiskBaseDocumentReferenceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskBaseDocumentID != null ? riskBaseDocumentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskbasedocument)) {
            return false;
        }
        Riskbasedocument other = (Riskbasedocument) object;
        if ((this.riskBaseDocumentID == null && other.riskBaseDocumentID != null) || (this.riskBaseDocumentID != null && !this.riskBaseDocumentID.equals(other.riskBaseDocumentID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskbasedocument[ riskBaseDocumentID=" + riskBaseDocumentID + " ]";
    }
    
}
