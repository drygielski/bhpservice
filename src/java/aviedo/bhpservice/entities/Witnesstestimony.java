/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "witnesstestimony")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Witnesstestimony.findAll", query = "SELECT w FROM Witnesstestimony w")
    , @NamedQuery(name = "Witnesstestimony.findByWinessTestimonyID", query = "SELECT w FROM Witnesstestimony w WHERE w.winessTestimonyID = :winessTestimonyID")
    , @NamedQuery(name = "Witnesstestimony.findByWitnessTestimonyDateTime", query = "SELECT w FROM Witnesstestimony w WHERE w.witnessTestimonyDateTime = :witnessTestimonyDateTime")})
public class Witnesstestimony implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "winessTestimonyID")
    private Integer winessTestimonyID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "witnessTestimonyContent")
    private String witnessTestimonyContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WitnessTestimonyDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date witnessTestimonyDateTime;
    @JoinColumn(name = "WitnessTestimonyIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident witnessTestimonyIncidentID;

    public Witnesstestimony() {
    }

    public Witnesstestimony(Integer winessTestimonyID) {
        this.winessTestimonyID = winessTestimonyID;
    }

    public Witnesstestimony(Integer winessTestimonyID, String witnessTestimonyContent, Date witnessTestimonyDateTime) {
        this.winessTestimonyID = winessTestimonyID;
        this.witnessTestimonyContent = witnessTestimonyContent;
        this.witnessTestimonyDateTime = witnessTestimonyDateTime;
    }

    public Integer getWinessTestimonyID() {
        return winessTestimonyID;
    }

    public void setWinessTestimonyID(Integer winessTestimonyID) {
        this.winessTestimonyID = winessTestimonyID;
    }

    public String getWitnessTestimonyContent() {
        return witnessTestimonyContent;
    }

    public void setWitnessTestimonyContent(String witnessTestimonyContent) {
        this.witnessTestimonyContent = witnessTestimonyContent;
    }

    public Date getWitnessTestimonyDateTime() {
        return witnessTestimonyDateTime;
    }

    public void setWitnessTestimonyDateTime(Date witnessTestimonyDateTime) {
        this.witnessTestimonyDateTime = witnessTestimonyDateTime;
    }

    public Incident getWitnessTestimonyIncidentID() {
        return witnessTestimonyIncidentID;
    }

    public void setWitnessTestimonyIncidentID(Incident witnessTestimonyIncidentID) {
        this.witnessTestimonyIncidentID = witnessTestimonyIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (winessTestimonyID != null ? winessTestimonyID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Witnesstestimony)) {
            return false;
        }
        Witnesstestimony other = (Witnesstestimony) object;
        if ((this.winessTestimonyID == null && other.winessTestimonyID != null) || (this.winessTestimonyID != null && !this.winessTestimonyID.equals(other.winessTestimonyID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Witnesstestimony[ winessTestimonyID=" + winessTestimonyID + " ]";
    }
    
}
