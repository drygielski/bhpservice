/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskanalysis")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskanalysis.findAll", query = "SELECT r FROM Riskanalysis r")
    , @NamedQuery(name = "Riskanalysis.findByRiskAnalysisID", query = "SELECT r FROM Riskanalysis r WHERE r.riskAnalysisID = :riskAnalysisID")
    , @NamedQuery(name = "Riskanalysis.findByRiskAnalysisName", query = "SELECT r FROM Riskanalysis r WHERE r.riskAnalysisName = :riskAnalysisName")
    , @NamedQuery(name = "Riskanalysis.findByRiskAnalysisVersion", query = "SELECT r FROM Riskanalysis r WHERE r.riskAnalysisVersion = :riskAnalysisVersion")
    , @NamedQuery(name = "Riskanalysis.findByRiskAnalysisCreationTimestamp", query = "SELECT r FROM Riskanalysis r WHERE r.riskAnalysisCreationTimestamp = :riskAnalysisCreationTimestamp")
    , @NamedQuery(name = "Riskanalysis.findByRiskAnalysisLastUpdateTimestamp", query = "SELECT r FROM Riskanalysis r WHERE r.riskAnalysisLastUpdateTimestamp = :riskAnalysisLastUpdateTimestamp")})
public class Riskanalysis implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskAnalysisID")
    private Integer riskAnalysisID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "riskAnalysisName")
    private String riskAnalysisName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "riskAnalysisVersion")
    private short riskAnalysisVersion;
    @Column(name = "riskAnalysisCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date riskAnalysisCreationTimestamp;
    @Column(name = "riskAnalysisLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date riskAnalysisLastUpdateTimestamp;
    @OneToMany(mappedBy = "employeeRiskAnalysisID")
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "riskAnalysisOHSSpecialistID", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User riskAnalysisOHSSpecialistID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskAnalysisID")
    private Collection<Riskidentification> riskidentificationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskBaseDocumentRiskAnalysisID")
    private Collection<Riskbasedocument> riskbasedocumentCollection;
    @OneToMany(mappedBy = "positionRiskAnalysisID")
    private Collection<Position> positionCollection;

    public Riskanalysis() {
    }

    public Riskanalysis(Integer riskAnalysisID) {
        this.riskAnalysisID = riskAnalysisID;
    }

    public Riskanalysis(Integer riskAnalysisID, String riskAnalysisName, short riskAnalysisVersion) {
        this.riskAnalysisID = riskAnalysisID;
        this.riskAnalysisName = riskAnalysisName;
        this.riskAnalysisVersion = riskAnalysisVersion;
    }

    public Integer getRiskAnalysisID() {
        return riskAnalysisID;
    }

    public void setRiskAnalysisID(Integer riskAnalysisID) {
        this.riskAnalysisID = riskAnalysisID;
    }

    public String getRiskAnalysisName() {
        return riskAnalysisName;
    }

    public void setRiskAnalysisName(String riskAnalysisName) {
        this.riskAnalysisName = riskAnalysisName;
    }

    public short getRiskAnalysisVersion() {
        return riskAnalysisVersion;
    }

    public void setRiskAnalysisVersion(short riskAnalysisVersion) {
        this.riskAnalysisVersion = riskAnalysisVersion;
    }

    public Date getRiskAnalysisCreationTimestamp() {
        return riskAnalysisCreationTimestamp;
    }

    public void setRiskAnalysisCreationTimestamp(Date riskAnalysisCreationTimestamp) {
        this.riskAnalysisCreationTimestamp = riskAnalysisCreationTimestamp;
    }

    public Date getRiskAnalysisLastUpdateTimestamp() {
        return riskAnalysisLastUpdateTimestamp;
    }

    public void setRiskAnalysisLastUpdateTimestamp(Date riskAnalysisLastUpdateTimestamp) {
        this.riskAnalysisLastUpdateTimestamp = riskAnalysisLastUpdateTimestamp;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public User getRiskAnalysisOHSSpecialistID() {
        return riskAnalysisOHSSpecialistID;
    }

    public void setRiskAnalysisOHSSpecialistID(User riskAnalysisOHSSpecialistID) {
        this.riskAnalysisOHSSpecialistID = riskAnalysisOHSSpecialistID;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @XmlTransient
    public Collection<Riskbasedocument> getRiskbasedocumentCollection() {
        return riskbasedocumentCollection;
    }

    public void setRiskbasedocumentCollection(Collection<Riskbasedocument> riskbasedocumentCollection) {
        this.riskbasedocumentCollection = riskbasedocumentCollection;
    }

    @XmlTransient
    public Collection<Position> getPositionCollection() {
        return positionCollection;
    }

    public void setPositionCollection(Collection<Position> positionCollection) {
        this.positionCollection = positionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskAnalysisID != null ? riskAnalysisID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskanalysis)) {
            return false;
        }
        Riskanalysis other = (Riskanalysis) object;
        if ((this.riskAnalysisID == null && other.riskAnalysisID != null) || (this.riskAnalysisID != null && !this.riskAnalysisID.equals(other.riskAnalysisID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskanalysis[ riskAnalysisID=" + riskAnalysisID + " ]";
    }
    
}
