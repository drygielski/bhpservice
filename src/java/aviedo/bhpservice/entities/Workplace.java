/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "workplace")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Workplace.findAll", query = "SELECT w FROM Workplace w")
    , @NamedQuery(name = "Workplace.findByWorkplaceID", query = "SELECT w FROM Workplace w WHERE w.workplaceID = :workplaceID")})
public class Workplace implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "workplaceID")
    private Integer workplaceID;
    @Lob
    @Size(max = 65535)
    @Column(name = "workplaceName")
    private String workplaceName;
    @Lob
    @Size(max = 65535)
    @Column(name = "workplaceDescription")
    private String workplaceDescription;
    @JoinColumn(name = "workplaceReferenceID", referencedColumnName = "workplaceReferenceID")
    @ManyToOne
    private Workplacereference workplaceReferenceID;
    @JoinColumn(name = "workplacePositionID", referencedColumnName = "positionID")
    @ManyToOne(optional = false)
    private Position workplacePositionID;

    public Workplace() {
    }

    public Workplace(Integer workplaceID) {
        this.workplaceID = workplaceID;
    }

    public Integer getWorkplaceID() {
        return workplaceID;
    }

    public void setWorkplaceID(Integer workplaceID) {
        this.workplaceID = workplaceID;
    }

    public String getWorkplaceName() {
        return workplaceName;
    }

    public void setWorkplaceName(String workplaceName) {
        this.workplaceName = workplaceName;
    }

    public String getWorkplaceDescription() {
        return workplaceDescription;
    }

    public void setWorkplaceDescription(String workplaceDescription) {
        this.workplaceDescription = workplaceDescription;
    }

    public Workplacereference getWorkplaceReferenceID() {
        return workplaceReferenceID;
    }

    public void setWorkplaceReferenceID(Workplacereference workplaceReferenceID) {
        this.workplaceReferenceID = workplaceReferenceID;
    }

    public Position getWorkplacePositionID() {
        return workplacePositionID;
    }

    public void setWorkplacePositionID(Position workplacePositionID) {
        this.workplacePositionID = workplacePositionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workplaceID != null ? workplaceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Workplace)) {
            return false;
        }
        Workplace other = (Workplace) object;
        if ((this.workplaceID == null && other.workplaceID != null) || (this.workplaceID != null && !this.workplaceID.equals(other.workplaceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Workplace[ workplaceID=" + workplaceID + " ]";
    }
    
}
