/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "educationalactivityreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Educationalactivityreference.findAll", query = "SELECT e FROM Educationalactivityreference e")
    , @NamedQuery(name = "Educationalactivityreference.findByEducationalActivityReferenceID", query = "SELECT e FROM Educationalactivityreference e WHERE e.educationalActivityReferenceID = :educationalActivityReferenceID")
    , @NamedQuery(name = "Educationalactivityreference.findByEducationalActivityReferenceShortName", query = "SELECT e FROM Educationalactivityreference e WHERE e.educationalActivityReferenceShortName = :educationalActivityReferenceShortName")
    , @NamedQuery(name = "Educationalactivityreference.findByEducationalActivityReferenceDuration", query = "SELECT e FROM Educationalactivityreference e WHERE e.educationalActivityReferenceDuration = :educationalActivityReferenceDuration")
    , @NamedQuery(name = "Educationalactivityreference.findByEducationalActivityReferenceInitial", query = "SELECT e FROM Educationalactivityreference e WHERE e.educationalActivityReferenceInitial = :educationalActivityReferenceInitial")})
public class Educationalactivityreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "educationalActivityReferenceID")
    private Short educationalActivityReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "educationalActivityReferenceShortName")
    private String educationalActivityReferenceShortName;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "educationalActivityReferenceName")
    private String educationalActivityReferenceName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "educationalActivityReferenceDuration")
    @Temporal(TemporalType.TIME)
    private Date educationalActivityReferenceDuration;
    @Basic(optional = false)
    @NotNull
    @Column(name = "educationalActivityReferenceInitial")
    private boolean educationalActivityReferenceInitial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "educationalActivityEducationalActivityReferenceID")
    private Collection<Educationalactivity> educationalactivityCollection;
    @JoinColumn(name = "educationalActivityReferenceWorkplaceID", referencedColumnName = "positionID")
    @ManyToOne
    private Position educationalActivityReferenceWorkplaceID;

    public Educationalactivityreference() {
    }

    public Educationalactivityreference(Short educationalActivityReferenceID) {
        this.educationalActivityReferenceID = educationalActivityReferenceID;
    }

    public Educationalactivityreference(Short educationalActivityReferenceID, String educationalActivityReferenceShortName, String educationalActivityReferenceName, Date educationalActivityReferenceDuration, boolean educationalActivityReferenceInitial) {
        this.educationalActivityReferenceID = educationalActivityReferenceID;
        this.educationalActivityReferenceShortName = educationalActivityReferenceShortName;
        this.educationalActivityReferenceName = educationalActivityReferenceName;
        this.educationalActivityReferenceDuration = educationalActivityReferenceDuration;
        this.educationalActivityReferenceInitial = educationalActivityReferenceInitial;
    }

    public Short getEducationalActivityReferenceID() {
        return educationalActivityReferenceID;
    }

    public void setEducationalActivityReferenceID(Short educationalActivityReferenceID) {
        this.educationalActivityReferenceID = educationalActivityReferenceID;
    }

    public String getEducationalActivityReferenceShortName() {
        return educationalActivityReferenceShortName;
    }

    public void setEducationalActivityReferenceShortName(String educationalActivityReferenceShortName) {
        this.educationalActivityReferenceShortName = educationalActivityReferenceShortName;
    }

    public String getEducationalActivityReferenceName() {
        return educationalActivityReferenceName;
    }

    public void setEducationalActivityReferenceName(String educationalActivityReferenceName) {
        this.educationalActivityReferenceName = educationalActivityReferenceName;
    }

    public Date getEducationalActivityReferenceDuration() {
        return educationalActivityReferenceDuration;
    }

    public void setEducationalActivityReferenceDuration(Date educationalActivityReferenceDuration) {
        this.educationalActivityReferenceDuration = educationalActivityReferenceDuration;
    }

    public boolean getEducationalActivityReferenceInitial() {
        return educationalActivityReferenceInitial;
    }

    public void setEducationalActivityReferenceInitial(boolean educationalActivityReferenceInitial) {
        this.educationalActivityReferenceInitial = educationalActivityReferenceInitial;
    }

    @XmlTransient
    public Collection<Educationalactivity> getEducationalactivityCollection() {
        return educationalactivityCollection;
    }

    public void setEducationalactivityCollection(Collection<Educationalactivity> educationalactivityCollection) {
        this.educationalactivityCollection = educationalactivityCollection;
    }

    public Position getEducationalActivityReferenceWorkplaceID() {
        return educationalActivityReferenceWorkplaceID;
    }

    public void setEducationalActivityReferenceWorkplaceID(Position educationalActivityReferenceWorkplaceID) {
        this.educationalActivityReferenceWorkplaceID = educationalActivityReferenceWorkplaceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (educationalActivityReferenceID != null ? educationalActivityReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Educationalactivityreference)) {
            return false;
        }
        Educationalactivityreference other = (Educationalactivityreference) object;
        if ((this.educationalActivityReferenceID == null && other.educationalActivityReferenceID != null) || (this.educationalActivityReferenceID != null && !this.educationalActivityReferenceID.equals(other.educationalActivityReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return educationalActivityReferenceShortName;
    }
    
}
