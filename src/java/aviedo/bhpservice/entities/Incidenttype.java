/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidenttype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidenttype.findAll", query = "SELECT i FROM Incidenttype i")
    , @NamedQuery(name = "Incidenttype.findByIncidentTypeID", query = "SELECT i FROM Incidenttype i WHERE i.incidentTypeID = :incidentTypeID")})
public class Incidenttype implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentTypeID")
    private Integer incidentTypeID;
    @JoinColumn(name = "incidentTypeIncidentTypeReferenceID", referencedColumnName = "incidentTypeReferenceID")
    @ManyToOne(optional = false)
    private Incidenttypereference incidentTypeIncidentTypeReferenceID;
    @JoinColumn(name = "incidentTypeIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident incidentTypeIncidentID;

    public Incidenttype() {
    }

    public Incidenttype(Integer incidentTypeID) {
        this.incidentTypeID = incidentTypeID;
    }

    public Integer getIncidentTypeID() {
        return incidentTypeID;
    }

    public void setIncidentTypeID(Integer incidentTypeID) {
        this.incidentTypeID = incidentTypeID;
    }

    public Incidenttypereference getIncidentTypeIncidentTypeReferenceID() {
        return incidentTypeIncidentTypeReferenceID;
    }

    public void setIncidentTypeIncidentTypeReferenceID(Incidenttypereference incidentTypeIncidentTypeReferenceID) {
        this.incidentTypeIncidentTypeReferenceID = incidentTypeIncidentTypeReferenceID;
    }

    public Incident getIncidentTypeIncidentID() {
        return incidentTypeIncidentID;
    }

    public void setIncidentTypeIncidentID(Incident incidentTypeIncidentID) {
        this.incidentTypeIncidentID = incidentTypeIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentTypeID != null ? incidentTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidenttype)) {
            return false;
        }
        Incidenttype other = (Incidenttype) object;
        if ((this.incidentTypeID == null && other.incidentTypeID != null) || (this.incidentTypeID != null && !this.incidentTypeID.equals(other.incidentTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidenttype[ incidentTypeID=" + incidentTypeID + " ]";
    }
    
}
