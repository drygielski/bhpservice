/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentconclusion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentconclusion.findAll", query = "SELECT i FROM Incidentconclusion i")
    , @NamedQuery(name = "Incidentconclusion.findByIncidentConclusionID", query = "SELECT i FROM Incidentconclusion i WHERE i.incidentConclusionID = :incidentConclusionID")})
public class Incidentconclusion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentConclusionID")
    private Integer incidentConclusionID;
    @Lob
    @Size(max = 65535)
    @Column(name = "incidentConclusionName")
    private String incidentConclusionName;
    @JoinColumn(name = "incidentConclusionIncidentConclusionReferenceID", referencedColumnName = "incidentConclusionReferenceID")
    @ManyToOne
    private Incidentconclusionreference incidentConclusionIncidentConclusionReferenceID;
    @JoinColumn(name = "incidentConclusionIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident incidentConclusionIncidentID;

    public Incidentconclusion() {
    }

    public Incidentconclusion(Integer incidentConclusionID) {
        this.incidentConclusionID = incidentConclusionID;
    }

    public Integer getIncidentConclusionID() {
        return incidentConclusionID;
    }

    public void setIncidentConclusionID(Integer incidentConclusionID) {
        this.incidentConclusionID = incidentConclusionID;
    }

    public String getIncidentConclusionName() {
        return incidentConclusionName;
    }

    public void setIncidentConclusionName(String incidentConclusionName) {
        this.incidentConclusionName = incidentConclusionName;
    }

    public Incidentconclusionreference getIncidentConclusionIncidentConclusionReferenceID() {
        return incidentConclusionIncidentConclusionReferenceID;
    }

    public void setIncidentConclusionIncidentConclusionReferenceID(Incidentconclusionreference incidentConclusionIncidentConclusionReferenceID) {
        this.incidentConclusionIncidentConclusionReferenceID = incidentConclusionIncidentConclusionReferenceID;
    }

    public Incident getIncidentConclusionIncidentID() {
        return incidentConclusionIncidentID;
    }

    public void setIncidentConclusionIncidentID(Incident incidentConclusionIncidentID) {
        this.incidentConclusionIncidentID = incidentConclusionIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentConclusionID != null ? incidentConclusionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentconclusion)) {
            return false;
        }
        Incidentconclusion other = (Incidentconclusion) object;
        if ((this.incidentConclusionID == null && other.incidentConclusionID != null) || (this.incidentConclusionID != null && !this.incidentConclusionID.equals(other.incidentConclusionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentconclusion[ incidentConclusionID=" + incidentConclusionID + " ]";
    }
    
}
