/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentcausereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentcausereference.findAll", query = "SELECT i FROM Incidentcausereference i")
    , @NamedQuery(name = "Incidentcausereference.findByIncidentCauseReferenceID", query = "SELECT i FROM Incidentcausereference i WHERE i.incidentCauseReferenceID = :incidentCauseReferenceID")})
public class Incidentcausereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentCauseReferenceID")
    private Short incidentCauseReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "incidentCauseReferenceName")
    private String incidentCauseReferenceName;
    @OneToMany(mappedBy = "incidentCauseIncidentCauseReferenceID")
    private Collection<Incidentcause> incidentcauseCollection;

    public Incidentcausereference() {
    }

    public Incidentcausereference(Short incidentCauseReferenceID) {
        this.incidentCauseReferenceID = incidentCauseReferenceID;
    }

    public Incidentcausereference(Short incidentCauseReferenceID, String incidentCauseReferenceName) {
        this.incidentCauseReferenceID = incidentCauseReferenceID;
        this.incidentCauseReferenceName = incidentCauseReferenceName;
    }

    public Short getIncidentCauseReferenceID() {
        return incidentCauseReferenceID;
    }

    public void setIncidentCauseReferenceID(Short incidentCauseReferenceID) {
        this.incidentCauseReferenceID = incidentCauseReferenceID;
    }

    public String getIncidentCauseReferenceName() {
        return incidentCauseReferenceName;
    }

    public void setIncidentCauseReferenceName(String incidentCauseReferenceName) {
        this.incidentCauseReferenceName = incidentCauseReferenceName;
    }

    @XmlTransient
    public Collection<Incidentcause> getIncidentcauseCollection() {
        return incidentcauseCollection;
    }

    public void setIncidentcauseCollection(Collection<Incidentcause> incidentcauseCollection) {
        this.incidentcauseCollection = incidentcauseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentCauseReferenceID != null ? incidentCauseReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentcausereference)) {
            return false;
        }
        Incidentcausereference other = (Incidentcausereference) object;
        if ((this.incidentCauseReferenceID == null && other.incidentCauseReferenceID != null) || (this.incidentCauseReferenceID != null && !this.incidentCauseReferenceID.equals(other.incidentCauseReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentcausereference[ incidentCauseReferenceID=" + incidentCauseReferenceID + " ]";
    }
    
}
