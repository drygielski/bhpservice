/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
    , @NamedQuery(name = "Employee.findByEmployeeCompanyID", query = "SELECT e FROM Employee e WHERE e.employeeCompanyID = :employeeCompanyID")
    , @NamedQuery(name = "Employee.findByEmployeeHiredDate", query = "SELECT e FROM Employee e WHERE e.employeeHiredDate = :employeeHiredDate")
    , @NamedQuery(name = "Employee.findByEmployeeEmployeer", query = "SELECT e FROM Employee e WHERE e.employeeEmployeer = :employeeEmployeer")
    , @NamedQuery(name = "Employee.findByEmployeeOHSFamiliarDate", query = "SELECT e FROM Employee e WHERE e.employeeOHSFamiliarDate = :employeeOHSFamiliarDate")
    , @NamedQuery(name = "Employee.findByEmployeeEqualOpportunitiesFamiliar", query = "SELECT e FROM Employee e WHERE e.employeeEqualOpportunitiesFamiliar = :employeeEqualOpportunitiesFamiliar")
    , @NamedQuery(name = "Employee.findByEmployeeCreationTimestamp", query = "SELECT e FROM Employee e WHERE e.employeeCreationTimestamp = :employeeCreationTimestamp")
    , @NamedQuery(name = "Employee.findByEmployeeLastUpdateTimestamp", query = "SELECT e FROM Employee e WHERE e.employeeLastUpdateTimestamp = :employeeLastUpdateTimestamp")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "employeeID")
    private Integer employeeID;
    @Column(name = "employeeHiredDate")
    @Temporal(TemporalType.DATE)
    private Date employeeHiredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "employeeEmployeer")
    private boolean employeeEmployeer;
    @Column(name = "employeeOHSFamiliarDate")
    @Temporal(TemporalType.DATE)
    private Date employeeOHSFamiliarDate;
    @Column(name = "employeeEqualOpportunitiesFamiliar")
    @Temporal(TemporalType.DATE)
    private Date employeeEqualOpportunitiesFamiliar;
    @Column(name = "employeeCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date employeeCreationTimestamp;
    @Column(name = "employeeLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date employeeLastUpdateTimestamp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trainingEmployeeID")
    private Collection<Training> trainingCollection;
    @JoinColumn(name = "employeeRiskAnalysisID", referencedColumnName = "riskAnalysisID")
    @ManyToOne
    private Riskanalysis employeeRiskAnalysisID;
    @JoinColumn(name = "employeeInitialTrainingID", referencedColumnName = "trainingID")
    @ManyToOne
    private Training employeeInitialTrainingID;
    @JoinColumn(name = "employeePeriodicTrainingID", referencedColumnName = "trainingID")
    @ManyToOne
    private Training employeePeriodicTrainingID;
    @JoinColumn(name = "employeeCompanyID", referencedColumnName = "companyID")
    @ManyToOne(optional = false)
    private Company employeeCompanyID;
    @JoinColumn(name = "employeePersonID", referencedColumnName = "personID")
    @ManyToOne(optional = false, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private Person employeePersonID;
    @JoinColumn(name = "employeeWorkplaceID", referencedColumnName = "positionID")
    @ManyToOne
    private Position employeeWorkplaceID;
    @OneToMany(mappedBy = "companyOwnerID")
    private Collection<Company> companyCollection;
    @OneToMany(mappedBy = "companyRepresentativeID")
    private Collection<Company> companyCollection1;
    @OneToMany(mappedBy = "incidentApplicantID")
    private Collection<Incident> incidentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentVictimID")
    private Collection<Incident> incidentCollection1;
    @OneToMany(mappedBy = "incidentCompanyRepresentativeID")
    private Collection<Incident> incidentCollection2;

    public Employee() {
    }

    public Employee(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public Employee(Integer employeeID, boolean employeeEmployeer) {
        this.employeeID = employeeID;
        this.employeeEmployeer = employeeEmployeer;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public Date getEmployeeHiredDate() {
        return employeeHiredDate;
    }

    public void setEmployeeHiredDate(Date employeeHiredDate) {
        this.employeeHiredDate = employeeHiredDate;
    }

    public boolean getEmployeeEmployeer() {
        return employeeEmployeer;
    }

    public void setEmployeeEmployeer(boolean employeeEmployeer) {
        this.employeeEmployeer = employeeEmployeer;
    }

    public Date getEmployeeOHSFamiliarDate() {
        return employeeOHSFamiliarDate;
    }

    public void setEmployeeOHSFamiliarDate(Date employeeOHSFamiliarDate) {
        this.employeeOHSFamiliarDate = employeeOHSFamiliarDate;
    }

    public Date getEmployeeEqualOpportunitiesFamiliar() {
        return employeeEqualOpportunitiesFamiliar;
    }

    public void setEmployeeEqualOpportunitiesFamiliar(Date employeeEqualOpportunitiesFamiliar) {
        this.employeeEqualOpportunitiesFamiliar = employeeEqualOpportunitiesFamiliar;
    }

    public Date getEmployeeCreationTimestamp() {
        return employeeCreationTimestamp;
    }

    public void setEmployeeCreationTimestamp(Date employeeCreationTimestamp) {
        this.employeeCreationTimestamp = employeeCreationTimestamp;
    }

    public Date getEmployeeLastUpdateTimestamp() {
        return employeeLastUpdateTimestamp;
    }

    public void setEmployeeLastUpdateTimestamp(Date employeeLastUpdateTimestamp) {
        this.employeeLastUpdateTimestamp = employeeLastUpdateTimestamp;
    }

    @XmlTransient
    public Collection<Training> getTrainingCollection() {
        return trainingCollection;
    }

    public void setTrainingCollection(Collection<Training> trainingCollection) {
        this.trainingCollection = trainingCollection;
    }

    public Riskanalysis getEmployeeRiskAnalysisID() {
        return employeeRiskAnalysisID;
    }

    public void setEmployeeRiskAnalysisID(Riskanalysis employeeRiskAnalysisID) {
        this.employeeRiskAnalysisID = employeeRiskAnalysisID;
    }

    public Training getEmployeeInitialTrainingID() {
        return employeeInitialTrainingID;
    }

    public void setEmployeeInitialTrainingID(Training employeeInitialTrainingID) {
        this.employeeInitialTrainingID = employeeInitialTrainingID;
    }

    public Training getEmployeePeriodicTrainingID() {
        return employeePeriodicTrainingID;
    }

    public void setEmployeePeriodicTrainingID(Training employeePeriodicTrainingID) {
        this.employeePeriodicTrainingID = employeePeriodicTrainingID;
    }

    public Company getEmployeeCompanyID() {
        return employeeCompanyID;
    }

    public void setEmployeeCompanyID(Company employeeCompanyID) {
        this.employeeCompanyID = employeeCompanyID;
    }

    public Person getEmployeePersonID() {
        return employeePersonID;
    }

    public void setEmployeePersonID(Person employeePersonID) {
        this.employeePersonID = employeePersonID;
    }

    public Position getEmployeeWorkplaceID() {
        return employeeWorkplaceID;
    }

    public void setEmployeeWorkplaceID(Position employeeWorkplaceID) {
        this.employeeWorkplaceID = employeeWorkplaceID;
    }

    @XmlTransient
    public Collection<Company> getCompanyCollection() {
        return companyCollection;
    }

    public void setCompanyCollection(Collection<Company> companyCollection) {
        this.companyCollection = companyCollection;
    }

    @XmlTransient
    public Collection<Company> getCompanyCollection1() {
        return companyCollection1;
    }

    public void setCompanyCollection1(Collection<Company> companyCollection1) {
        this.companyCollection1 = companyCollection1;
    }

    @XmlTransient
    public Collection<Incident> getIncidentCollection() {
        return incidentCollection;
    }

    public void setIncidentCollection(Collection<Incident> incidentCollection) {
        this.incidentCollection = incidentCollection;
    }

    @XmlTransient
    public Collection<Incident> getIncidentCollection1() {
        return incidentCollection1;
    }

    public void setIncidentCollection1(Collection<Incident> incidentCollection1) {
        this.incidentCollection1 = incidentCollection1;
    }

    @XmlTransient
    public Collection<Incident> getIncidentCollection2() {
        return incidentCollection2;
    }

    public void setIncidentCollection2(Collection<Incident> incidentCollection2) {
        this.incidentCollection2 = incidentCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeID != null ? employeeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.employeeID == null && other.employeeID != null) || (this.employeeID != null && !this.employeeID.equals(other.employeeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return employeePersonID.getDescription();
    }
    
}
