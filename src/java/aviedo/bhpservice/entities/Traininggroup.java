/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "traininggroup")
@XmlRootElement
public class Traininggroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trainingGroupID")
    private Integer trainingGroupID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "trainingGroupRegisterNumber")
    private int trainingGroupRegisterNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "trainingInitial")
    private boolean trainingInitial;
    @Column(name = "trainingGroupStartDate")
    @Temporal(TemporalType.DATE)
    private Date trainingGroupStartDate;
    @Column(name = "trainingGroupEndDate")
    @Temporal(TemporalType.DATE)
    private Date trainingGroupEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "trainingGroupValidityMonths")
    private short trainingGroupValidityMonths;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "educationalActivityTrainingGroupID")
    private Collection<Educationalactivity> educationalactivityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trainingGroupTrainingGroupID")
    private Collection<Training> trainingCollection;
    @JoinColumn(name = "trainingGroupOrganizerID", referencedColumnName = "userID")
    @ManyToOne(optional = false)
    private User trainingGroupOrganizerID;

    public Traininggroup() {
    }

    public Traininggroup(Integer trainingGroupID) {
        this.trainingGroupID = trainingGroupID;
    }

    public Traininggroup(Integer trainingGroupID, int trainingGroupRegisterNumber, 
            boolean trainingInitial, short trainingGroupValidityMonths) {
        this.trainingGroupID = trainingGroupID;
        this.trainingGroupRegisterNumber = trainingGroupRegisterNumber;
        this.trainingInitial = trainingInitial;
        this.trainingGroupValidityMonths = trainingGroupValidityMonths;
    }

    public Integer getTrainingGroupID() {
        return trainingGroupID;
    }

    public void setTrainingGroupID(Integer trainingGroupID) {
        this.trainingGroupID = trainingGroupID;
    }

    public int getTrainingGroupRegisterNumber() {
        return trainingGroupRegisterNumber;
    }

    public void setTrainingGroupRegisterNumber(int trainingGroupRegisterNumber) {
        this.trainingGroupRegisterNumber = trainingGroupRegisterNumber;
    }

    public boolean getTrainingInitial() {
        return trainingInitial;
    }

    public void setTrainingInitial(boolean trainingInitial) {
        this.trainingInitial = trainingInitial;
    }

    public Date getTrainingGroupStartDate() {
        return trainingGroupStartDate;
    }

    public void setTrainingGroupStartDate(Date trainingGroupStartDate) {
        this.trainingGroupStartDate = trainingGroupStartDate;
    }

    public Date getTrainingGroupEndDate() {
        return trainingGroupEndDate;
    }

    public void setTrainingGroupEndDate(Date trainingGroupEndDate) {
        this.trainingGroupEndDate = trainingGroupEndDate;
    }

    public short getTrainingGroupValidityMonths() {
        return trainingGroupValidityMonths;
    }

    public void setTrainingGroupValidityMonths(short trainingGroupValidityMonths) {
        this.trainingGroupValidityMonths = trainingGroupValidityMonths;
    }

    @XmlTransient
    public Collection<Educationalactivity> getEducationalactivityCollection() {
        return educationalactivityCollection;
    }

    public void setEducationalactivityCollection(Collection<Educationalactivity> educationalactivityCollection) {
        this.educationalactivityCollection = educationalactivityCollection;
    }

    @XmlTransient
    public Collection<Training> getTrainingCollection() {
        return trainingCollection;
    }

    public void setTrainingCollection(Collection<Training> trainingCollection) {
        this.trainingCollection = trainingCollection;
    }

    public User getTrainingGroupOrganizerID() {
        return trainingGroupOrganizerID;
    }

    public void setTrainingGroupOrganizerID(User trainingGroupOrganizerID) {
        this.trainingGroupOrganizerID = trainingGroupOrganizerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trainingGroupID != null ? trainingGroupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Traininggroup)) {
            return false;
        }
        Traininggroup other = (Traininggroup) object;
        if ((this.trainingGroupID == null && other.trainingGroupID != null) || (this.trainingGroupID != null && !this.trainingGroupID.equals(other.trainingGroupID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Traininggroup[ trainingGroupID=" + trainingGroupID + " ]";
    }
    
    public Date getExpiredDate() {
        if(trainingGroupEndDate != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(trainingGroupEndDate);
            cal.add(Calendar.MONTH, trainingGroupValidityMonths);
            return cal.getTime();
        }
        return trainingGroupEndDate;
    }
}
