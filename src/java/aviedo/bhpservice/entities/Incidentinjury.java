/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "incidentinjury")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incidentinjury.findAll", query = "SELECT i FROM Incidentinjury i")
    , @NamedQuery(name = "Incidentinjury.findByIncidentInjuryID", query = "SELECT i FROM Incidentinjury i WHERE i.incidentInjuryID = :incidentInjuryID")})
public class Incidentinjury implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentInjuryID")
    private Integer incidentInjuryID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "incidentInjuryType")
    private String incidentInjuryType;
    @JoinColumn(name = "incidentInjuryIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident incidentInjuryIncidentID;

    public Incidentinjury() {
    }

    public Incidentinjury(Integer incidentInjuryID) {
        this.incidentInjuryID = incidentInjuryID;
    }

    public Incidentinjury(Integer incidentInjuryID, String incidentInjuryType) {
        this.incidentInjuryID = incidentInjuryID;
        this.incidentInjuryType = incidentInjuryType;
    }

    public Integer getIncidentInjuryID() {
        return incidentInjuryID;
    }

    public void setIncidentInjuryID(Integer incidentInjuryID) {
        this.incidentInjuryID = incidentInjuryID;
    }

    public String getIncidentInjuryType() {
        return incidentInjuryType;
    }

    public void setIncidentInjuryType(String incidentInjuryType) {
        this.incidentInjuryType = incidentInjuryType;
    }

    public Incident getIncidentInjuryIncidentID() {
        return incidentInjuryIncidentID;
    }

    public void setIncidentInjuryIncidentID(Incident incidentInjuryIncidentID) {
        this.incidentInjuryIncidentID = incidentInjuryIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentInjuryID != null ? incidentInjuryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incidentinjury)) {
            return false;
        }
        Incidentinjury other = (Incidentinjury) object;
        if ((this.incidentInjuryID == null && other.incidentInjuryID != null) || (this.incidentInjuryID != null && !this.incidentInjuryID.equals(other.incidentInjuryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Incidentinjury[ incidentInjuryID=" + incidentInjuryID + " ]";
    }
    
}
