/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "victimtestimony")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Victimtestimony.findAll", query = "SELECT v FROM Victimtestimony v")
    , @NamedQuery(name = "Victimtestimony.findByVictimTestimonyID", query = "SELECT v FROM Victimtestimony v WHERE v.victimTestimonyID = :victimTestimonyID")
    , @NamedQuery(name = "Victimtestimony.findByVictimTestimonyDateTime", query = "SELECT v FROM Victimtestimony v WHERE v.victimTestimonyDateTime = :victimTestimonyDateTime")})
public class Victimtestimony implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "victimTestimonyID")
    private Integer victimTestimonyID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "victimTestimonyContent")
    private String victimTestimonyContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "victimTestimonyDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date victimTestimonyDateTime;
    @JoinColumn(name = "victimTestimonyIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident victimTestimonyIncidentID;

    public Victimtestimony() {
    }

    public Victimtestimony(Integer victimTestimonyID) {
        this.victimTestimonyID = victimTestimonyID;
    }

    public Victimtestimony(Integer victimTestimonyID, String victimTestimonyContent, Date victimTestimonyDateTime) {
        this.victimTestimonyID = victimTestimonyID;
        this.victimTestimonyContent = victimTestimonyContent;
        this.victimTestimonyDateTime = victimTestimonyDateTime;
    }

    public Integer getVictimTestimonyID() {
        return victimTestimonyID;
    }

    public void setVictimTestimonyID(Integer victimTestimonyID) {
        this.victimTestimonyID = victimTestimonyID;
    }

    public String getVictimTestimonyContent() {
        return victimTestimonyContent;
    }

    public void setVictimTestimonyContent(String victimTestimonyContent) {
        this.victimTestimonyContent = victimTestimonyContent;
    }

    public Date getVictimTestimonyDateTime() {
        return victimTestimonyDateTime;
    }

    public void setVictimTestimonyDateTime(Date victimTestimonyDateTime) {
        this.victimTestimonyDateTime = victimTestimonyDateTime;
    }

    public Incident getVictimTestimonyIncidentID() {
        return victimTestimonyIncidentID;
    }

    public void setVictimTestimonyIncidentID(Incident victimTestimonyIncidentID) {
        this.victimTestimonyIncidentID = victimTestimonyIncidentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (victimTestimonyID != null ? victimTestimonyID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Victimtestimony)) {
            return false;
        }
        Victimtestimony other = (Victimtestimony) object;
        if ((this.victimTestimonyID == null && other.victimTestimonyID != null) || (this.victimTestimonyID != null && !this.victimTestimonyID.equals(other.victimTestimonyID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Victimtestimony[ victimTestimonyID=" + victimTestimonyID + " ]";
    }
    
}
