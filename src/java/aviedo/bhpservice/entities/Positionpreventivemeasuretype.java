/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionpreventivemeasuretype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionpreventivemeasuretype.findAll", query = "SELECT p FROM Positionpreventivemeasuretype p")
    , @NamedQuery(name = "Positionpreventivemeasuretype.findByPositionPreventiveMeasureTypeID", query = "SELECT p FROM Positionpreventivemeasuretype p WHERE p.positionPreventiveMeasureTypeID = :positionPreventiveMeasureTypeID")
    , @NamedQuery(name = "Positionpreventivemeasuretype.findByPositionPreventiveMeasureTypeName", query = "SELECT p FROM Positionpreventivemeasuretype p WHERE p.positionPreventiveMeasureTypeName = :positionPreventiveMeasureTypeName")})
public class Positionpreventivemeasuretype implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionPreventiveMeasureTypeID")
    private Short positionPreventiveMeasureTypeID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "positionPreventiveMeasureTypeName")
    private String positionPreventiveMeasureTypeName;
    @OneToMany(mappedBy = "positionPreventiveMeasureTypeID")
    private Collection<Positionpreventivemeasure> positionpreventivemeasureCollection;

    public Positionpreventivemeasuretype() {
    }

    public Positionpreventivemeasuretype(Short positionPreventiveMeasureTypeID) {
        this.positionPreventiveMeasureTypeID = positionPreventiveMeasureTypeID;
    }

    public Positionpreventivemeasuretype(Short positionPreventiveMeasureTypeID, String positionPreventiveMeasureTypeName) {
        this.positionPreventiveMeasureTypeID = positionPreventiveMeasureTypeID;
        this.positionPreventiveMeasureTypeName = positionPreventiveMeasureTypeName;
    }

    public Short getPositionPreventiveMeasureTypeID() {
        return positionPreventiveMeasureTypeID;
    }

    public void setPositionPreventiveMeasureTypeID(Short positionPreventiveMeasureTypeID) {
        this.positionPreventiveMeasureTypeID = positionPreventiveMeasureTypeID;
    }

    public String getPositionPreventiveMeasureTypeName() {
        return positionPreventiveMeasureTypeName;
    }

    public void setPositionPreventiveMeasureTypeName(String positionPreventiveMeasureTypeName) {
        this.positionPreventiveMeasureTypeName = positionPreventiveMeasureTypeName;
    }

    @XmlTransient
    public Collection<Positionpreventivemeasure> getPositionpreventivemeasureCollection() {
        return positionpreventivemeasureCollection;
    }

    public void setPositionpreventivemeasureCollection(Collection<Positionpreventivemeasure> positionpreventivemeasureCollection) {
        this.positionpreventivemeasureCollection = positionpreventivemeasureCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionPreventiveMeasureTypeID != null ? positionPreventiveMeasureTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionpreventivemeasuretype)) {
            return false;
        }
        Positionpreventivemeasuretype other = (Positionpreventivemeasuretype) object;
        if ((this.positionPreventiveMeasureTypeID == null && other.positionPreventiveMeasureTypeID != null) || (this.positionPreventiveMeasureTypeID != null && !this.positionPreventiveMeasureTypeID.equals(other.positionPreventiveMeasureTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Positionpreventivemeasuretype[ positionPreventiveMeasureTypeID=" + positionPreventiveMeasureTypeID + " ]";
    }
    
}
