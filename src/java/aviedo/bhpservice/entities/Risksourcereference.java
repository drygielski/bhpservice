/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "risksourcereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Risksourcereference.findAll", query = "SELECT r FROM Risksourcereference r")
    , @NamedQuery(name = "Risksourcereference.findByRiskSourceReferenceID", query = "SELECT r FROM Risksourcereference r WHERE r.riskSourceReferenceID = :riskSourceReferenceID")})
public class Risksourcereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskSourceReferenceID")
    private Integer riskSourceReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "riskSourceReferenceName")
    private String riskSourceReferenceName;
    @JoinColumn(name = "riskSourceReferenceRiskReferenceID", referencedColumnName = "riskReferenceID")
    @ManyToOne(optional = false)
    private Riskreference riskSourceReferenceRiskReferenceID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskSourceReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;

    public Risksourcereference() {
    }

    public Risksourcereference(Integer riskSourceReferenceID) {
        this.riskSourceReferenceID = riskSourceReferenceID;
    }

    public Risksourcereference(Integer riskSourceReferenceID, String riskSourceReferenceName) {
        this.riskSourceReferenceID = riskSourceReferenceID;
        this.riskSourceReferenceName = riskSourceReferenceName;
    }

    public Integer getRiskSourceReferenceID() {
        return riskSourceReferenceID;
    }

    public void setRiskSourceReferenceID(Integer riskSourceReferenceID) {
        this.riskSourceReferenceID = riskSourceReferenceID;
    }

    public String getRiskSourceReferenceName() {
        return riskSourceReferenceName;
    }

    public void setRiskSourceReferenceName(String riskSourceReferenceName) {
        this.riskSourceReferenceName = riskSourceReferenceName;
    }

    public Riskreference getRiskSourceReferenceRiskReferenceID() {
        return riskSourceReferenceRiskReferenceID;
    }

    public void setRiskSourceReferenceRiskReferenceID(Riskreference riskSourceReferenceRiskReferenceID) {
        this.riskSourceReferenceRiskReferenceID = riskSourceReferenceRiskReferenceID;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskSourceReferenceID != null ? riskSourceReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Risksourcereference)) {
            return false;
        }
        Risksourcereference other = (Risksourcereference) object;
        if ((this.riskSourceReferenceID == null && other.riskSourceReferenceID != null) || (this.riskSourceReferenceID != null && !this.riskSourceReferenceID.equals(other.riskSourceReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Risksourcereference[ riskSourceReferenceID=" + riskSourceReferenceID + " ]";
    }
    
}
