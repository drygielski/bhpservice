/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionresponsibility")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionresponsibility.findAll", query = "SELECT p FROM Positionresponsibility p")
    , @NamedQuery(name = "Positionresponsibility.findByPositionResponsibilityID", query = "SELECT p FROM Positionresponsibility p WHERE p.positionResponsibilityID = :positionResponsibilityID")})
public class Positionresponsibility implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionResponsibilityID")
    private Integer positionResponsibilityID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "positionResponsibilityName")
    private String positionResponsibilityName;
    @JoinColumn(name = "positionResponsibilityPositionID", referencedColumnName = "positionID")
    @ManyToOne(optional = false)
    private Position positionResponsibilityPositionID;

    public Positionresponsibility() {
    }

    public Positionresponsibility(Integer positionResponsibilityID) {
        this.positionResponsibilityID = positionResponsibilityID;
    }

    public Positionresponsibility(Integer positionResponsibilityID, String positionResponsibilityName) {
        this.positionResponsibilityID = positionResponsibilityID;
        this.positionResponsibilityName = positionResponsibilityName;
    }

    public Integer getPositionResponsibilityID() {
        return positionResponsibilityID;
    }

    public void setPositionResponsibilityID(Integer positionResponsibilityID) {
        this.positionResponsibilityID = positionResponsibilityID;
    }

    public String getPositionResponsibilityName() {
        return positionResponsibilityName;
    }

    public void setPositionResponsibilityName(String positionResponsibilityName) {
        this.positionResponsibilityName = positionResponsibilityName;
    }

    public Position getPositionResponsibilityPositionID() {
        return positionResponsibilityPositionID;
    }

    public void setPositionResponsibilityPositionID(Position positionResponsibilityPositionID) {
        this.positionResponsibilityPositionID = positionResponsibilityPositionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionResponsibilityID != null ? positionResponsibilityID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionresponsibility)) {
            return false;
        }
        Positionresponsibility other = (Positionresponsibility) object;
        if ((this.positionResponsibilityID == null && other.positionResponsibilityID != null) || (this.positionResponsibilityID != null && !this.positionResponsibilityID.equals(other.positionResponsibilityID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Positionresponsibility[ positionResponsibilityID=" + positionResponsibilityID + " ]";
    }
    
}
