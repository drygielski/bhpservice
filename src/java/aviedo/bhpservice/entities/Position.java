/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "position")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Position.findAll", query = "SELECT p FROM Position p")
    , @NamedQuery(name = "Position.findByPositionID", query = "SELECT p FROM Position p WHERE p.positionID = :positionID")
    , @NamedQuery(name = "Position.findByPositionName", query = "SELECT p FROM Position p WHERE p.positionName = :positionName")
    , @NamedQuery(name = "Position.findByPositionWorkTimePerWeek", query = "SELECT p FROM Position p WHERE p.positionWorkTimePerWeek = :positionWorkTimePerWeek")
    , @NamedQuery(name = "Position.findByPositionShiftPlan", query = "SELECT p FROM Position p WHERE p.positionShiftPlan = :positionShiftPlan")})
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionID")
    private Integer positionID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "positionName")
    private String positionName;
    @Column(name = "positionWorkTimePerWeek")
    private Short positionWorkTimePerWeek;
    @Size(max = 45)
    @Column(name = "positionShiftPlan")
    private String positionShiftPlan;
    @OneToMany(mappedBy = "educationalActivityReferenceWorkplaceID")
    private Collection<Educationalactivityreference> educationalactivityreferenceCollection;
    @OneToMany(mappedBy = "employeeWorkplaceID")
    private Collection<Employee> employeeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "positionPreventiveMeasurePositionID")
    private Collection<Positionpreventivemeasure> positionpreventivemeasureCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "positionResponsibilityPositionID")
    private Collection<Positionresponsibility> positionresponsibilityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "positionDutyPositionID")
    private Collection<Positionduty> positiondutyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "positionQualificationPositionID")
    private Collection<Positionqualification> positionqualificationCollection;
    @JoinColumn(name = "positionProffesionClassificationCodeID", referencedColumnName = "proffesionClassificationCodeID")
    @ManyToOne
    private Proffesionclassificationcode positionProffesionClassificationCodeID;
    @JoinColumn(name = "positionRiskAnalysisID", referencedColumnName = "riskAnalysisID")
    @ManyToOne
    private Riskanalysis positionRiskAnalysisID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "workplacePositionID")
    private Collection<Workplace> workplaceCollection;

    public Position() {
    }

    public Position(Integer positionID) {
        this.positionID = positionID;
    }

    public Position(Integer positionID, String positionName) {
        this.positionID = positionID;
        this.positionName = positionName;
    }

    public Integer getPositionID() {
        return positionID;
    }

    public void setPositionID(Integer positionID) {
        this.positionID = positionID;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Short getPositionWorkTimePerWeek() {
        return positionWorkTimePerWeek;
    }

    public void setPositionWorkTimePerWeek(Short positionWorkTimePerWeek) {
        this.positionWorkTimePerWeek = positionWorkTimePerWeek;
    }

    public String getPositionShiftPlan() {
        return positionShiftPlan;
    }

    public void setPositionShiftPlan(String positionShiftPlan) {
        this.positionShiftPlan = positionShiftPlan;
    }

    @XmlTransient
    public Collection<Educationalactivityreference> getEducationalactivityreferenceCollection() {
        return educationalactivityreferenceCollection;
    }

    public void setEducationalactivityreferenceCollection(Collection<Educationalactivityreference> educationalactivityreferenceCollection) {
        this.educationalactivityreferenceCollection = educationalactivityreferenceCollection;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    @XmlTransient
    public Collection<Positionpreventivemeasure> getPositionpreventivemeasureCollection() {
        return positionpreventivemeasureCollection;
    }

    public void setPositionpreventivemeasureCollection(Collection<Positionpreventivemeasure> positionpreventivemeasureCollection) {
        this.positionpreventivemeasureCollection = positionpreventivemeasureCollection;
    }

    @XmlTransient
    public Collection<Positionresponsibility> getPositionresponsibilityCollection() {
        return positionresponsibilityCollection;
    }

    public void setPositionresponsibilityCollection(Collection<Positionresponsibility> positionresponsibilityCollection) {
        this.positionresponsibilityCollection = positionresponsibilityCollection;
    }

    @XmlTransient
    public Collection<Positionduty> getPositiondutyCollection() {
        return positiondutyCollection;
    }

    public void setPositiondutyCollection(Collection<Positionduty> positiondutyCollection) {
        this.positiondutyCollection = positiondutyCollection;
    }

    @XmlTransient
    public Collection<Positionqualification> getPositionqualificationCollection() {
        return positionqualificationCollection;
    }

    public void setPositionqualificationCollection(Collection<Positionqualification> positionqualificationCollection) {
        this.positionqualificationCollection = positionqualificationCollection;
    }

    public Proffesionclassificationcode getPositionProffesionClassificationCodeID() {
        return positionProffesionClassificationCodeID;
    }

    public void setPositionProffesionClassificationCodeID(Proffesionclassificationcode positionProffesionClassificationCodeID) {
        this.positionProffesionClassificationCodeID = positionProffesionClassificationCodeID;
    }

    public Riskanalysis getPositionRiskAnalysisID() {
        return positionRiskAnalysisID;
    }

    public void setPositionRiskAnalysisID(Riskanalysis positionRiskAnalysisID) {
        this.positionRiskAnalysisID = positionRiskAnalysisID;
    }

    @XmlTransient
    public Collection<Workplace> getWorkplaceCollection() {
        return workplaceCollection;
    }

    public void setWorkplaceCollection(Collection<Workplace> workplaceCollection) {
        this.workplaceCollection = workplaceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionID != null ? positionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Position)) {
            return false;
        }
        Position other = (Position) object;
        if ((this.positionID == null && other.positionID != null) || (this.positionID != null && !this.positionID.equals(other.positionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return positionName;
    }
    
}
