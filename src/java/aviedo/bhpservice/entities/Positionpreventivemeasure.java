/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "positionpreventivemeasure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positionpreventivemeasure.findAll", query = "SELECT p FROM Positionpreventivemeasure p")
    , @NamedQuery(name = "Positionpreventivemeasure.findByPositionPreventiveMeasureID", query = "SELECT p FROM Positionpreventivemeasure p WHERE p.positionPreventiveMeasureID = :positionPreventiveMeasureID")})
public class Positionpreventivemeasure implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "positionPreventiveMeasureID")
    private Integer positionPreventiveMeasureID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "positionPreventiveMeasureName")
    private String positionPreventiveMeasureName;
    @JoinColumn(name = "positionPreventiveMeasureTypeID", referencedColumnName = "positionPreventiveMeasureTypeID")
    @ManyToOne
    private Positionpreventivemeasuretype positionPreventiveMeasureTypeID;
    @JoinColumn(name = "positionPreventiveMeasurePositionID", referencedColumnName = "positionID")
    @ManyToOne(optional = false)
    private Position positionPreventiveMeasurePositionID;

    public Positionpreventivemeasure() {
    }

    public Positionpreventivemeasure(Integer positionPreventiveMeasureID) {
        this.positionPreventiveMeasureID = positionPreventiveMeasureID;
    }

    public Positionpreventivemeasure(Integer positionPreventiveMeasureID, String positionPreventiveMeasureName) {
        this.positionPreventiveMeasureID = positionPreventiveMeasureID;
        this.positionPreventiveMeasureName = positionPreventiveMeasureName;
    }

    public Integer getPositionPreventiveMeasureID() {
        return positionPreventiveMeasureID;
    }

    public void setPositionPreventiveMeasureID(Integer positionPreventiveMeasureID) {
        this.positionPreventiveMeasureID = positionPreventiveMeasureID;
    }

    public String getPositionPreventiveMeasureName() {
        return positionPreventiveMeasureName;
    }

    public void setPositionPreventiveMeasureName(String positionPreventiveMeasureName) {
        this.positionPreventiveMeasureName = positionPreventiveMeasureName;
    }

    public Positionpreventivemeasuretype getPositionPreventiveMeasureTypeID() {
        return positionPreventiveMeasureTypeID;
    }

    public void setPositionPreventiveMeasureTypeID(Positionpreventivemeasuretype positionPreventiveMeasureTypeID) {
        this.positionPreventiveMeasureTypeID = positionPreventiveMeasureTypeID;
    }

    public Position getPositionPreventiveMeasurePositionID() {
        return positionPreventiveMeasurePositionID;
    }

    public void setPositionPreventiveMeasurePositionID(Position positionPreventiveMeasurePositionID) {
        this.positionPreventiveMeasurePositionID = positionPreventiveMeasurePositionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionPreventiveMeasureID != null ? positionPreventiveMeasureID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positionpreventivemeasure)) {
            return false;
        }
        Positionpreventivemeasure other = (Positionpreventivemeasure) object;
        if ((this.positionPreventiveMeasureID == null && other.positionPreventiveMeasureID != null) || (this.positionPreventiveMeasureID != null && !this.positionPreventiveMeasureID.equals(other.positionPreventiveMeasureID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Positionpreventivemeasure[ positionPreventiveMeasureID=" + positionPreventiveMeasureID + " ]";
    }
    
}
