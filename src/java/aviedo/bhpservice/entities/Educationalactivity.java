/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "educationalactivity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Educationalactivity.findAll", query = "SELECT e FROM Educationalactivity e")
    , @NamedQuery(name = "Educationalactivity.findByEducationalActivityID", query = "SELECT e FROM Educationalactivity e WHERE e.educationalActivityID = :educationalActivityID")})
public class Educationalactivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "educationalActivityID")
    private Integer educationalActivityID;
    @JoinColumn(name = "educationalActivityEducationalActivityReferenceID", referencedColumnName = "educationalActivityReferenceID")
    @ManyToOne(optional = false)
    private Educationalactivityreference educationalActivityEducationalActivityReferenceID;
    @JoinColumn(name = "educationalActivityTrainingGroupID", referencedColumnName = "trainingGroupID")
    @ManyToOne(optional = false)
    private Traininggroup educationalActivityTrainingGroupID;

    public Educationalactivity() {
    }

    public Educationalactivity(Integer educationalActivityID) {
        this.educationalActivityID = educationalActivityID;
    }

    public Integer getEducationalActivityID() {
        return educationalActivityID;
    }

    public void setEducationalActivityID(Integer educationalActivityID) {
        this.educationalActivityID = educationalActivityID;
    }

    public Educationalactivityreference getEducationalActivityEducationalActivityReferenceID() {
        return educationalActivityEducationalActivityReferenceID;
    }

    public void setEducationalActivityEducationalActivityReferenceID(Educationalactivityreference educationalActivityEducationalActivityReferenceID) {
        this.educationalActivityEducationalActivityReferenceID = educationalActivityEducationalActivityReferenceID;
    }

    public Traininggroup getEducationalActivityTrainingGroupID() {
        return educationalActivityTrainingGroupID;
    }

    public void setEducationalActivityTrainingGroupID(Traininggroup educationalActivityTrainingGroupID) {
        this.educationalActivityTrainingGroupID = educationalActivityTrainingGroupID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (educationalActivityID != null ? educationalActivityID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Educationalactivity)) {
            return false;
        }
        Educationalactivity other = (Educationalactivity) object;
        if ((this.educationalActivityID == null && other.educationalActivityID != null) || (this.educationalActivityID != null && !this.educationalActivityID.equals(other.educationalActivityID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Educationalactivity[ educationalActivityID=" + educationalActivityID + " ]";
    }
    
}
