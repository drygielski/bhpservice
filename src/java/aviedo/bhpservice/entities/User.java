/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
    , @NamedQuery(name = "User.findByUserID", query = "SELECT u FROM User u WHERE u.userID = :userID")
    , @NamedQuery(name = "User.findByUserLogin", query = "SELECT u FROM User u WHERE u.userLogin = :userLogin")
    , @NamedQuery(name = "User.findByUserIsActive", query = "SELECT u FROM User u WHERE u.userIsActive = :userIsActive")
    , @NamedQuery(name = "User.findByUserEmail", query = "SELECT u FROM User u WHERE u.userEmail = :userEmail")
    , @NamedQuery(name = "User.findByUserPathPhoto", query = "SELECT u FROM User u WHERE u.userPathPhoto = :userPathPhoto")
    , @NamedQuery(name = "User.findByUserRole", query = "SELECT u FROM User u WHERE u.userRole = :userRole")
    , @NamedQuery(name = "User.findByUserLastLoginTimeStamp", query = "SELECT u FROM User u WHERE u.userLastLoginTimeStamp = :userLastLoginTimeStamp")
    , @NamedQuery(name = "User.findByUserCreationTimestamp", query = "SELECT u FROM User u WHERE u.userCreationTimestamp = :userCreationTimestamp")
    , @NamedQuery(name = "User.findByUserLastUpdateTimestamp", query = "SELECT u FROM User u WHERE u.userLastUpdateTimestamp = :userLastUpdateTimestamp")})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userID")
    private Integer userID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "userLogin")
    private String userLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userIsActive")
    private boolean userIsActive;
    @Size(max = 45)
    @Column(name = "userEmail")
    private String userEmail;
    @Size(max = 45)
    @Column(name = "userPathPhoto")
    private String userPathPhoto;
    @Size(max = 45)
    @Column(name = "userRole")
    private String userRole;
    @Column(name = "userLastLoginTimeStamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date userLastLoginTimeStamp;
    @Column(name = "userCreationTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date userCreationTimestamp;
    @Column(name = "userLastUpdateTimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date userLastUpdateTimestamp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientAdminID")
    private Collection<Client> clientCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskAnalysisOHSSpecialistID")
    private Collection<Riskanalysis> riskanalysisCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trainingGroupOrganizerID")
    private Collection<Traininggroup> traininggroupCollection;
    @OneToMany(mappedBy = "incidentOHSSpecialistID")
    private Collection<Incident> incidentCollection;
    @JoinColumn(name = "userClientID", referencedColumnName = "clientID")
    @ManyToOne
    private Client userClientID;
    @JoinColumn(name = "userPersonID", referencedColumnName = "personID")
    @ManyToOne
    private Person userPersonID;

    public User() {
    }

    public User(Integer userID) {
        this.userID = userID;
    }

    public User(Integer userID, String userLogin, boolean userIsActive) {
        this.userID = userID;
        this.userLogin = userLogin;
        this.userIsActive = userIsActive;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public boolean getUserIsActive() {
        return userIsActive;
    }

    public void setUserIsActive(boolean userIsActive) {
        this.userIsActive = userIsActive;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPathPhoto() {
        return userPathPhoto;
    }

    public void setUserPathPhoto(String userPathPhoto) {
        this.userPathPhoto = userPathPhoto;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Date getUserLastLoginTimeStamp() {
        return userLastLoginTimeStamp;
    }

    public void setUserLastLoginTimeStamp(Date userLastLoginTimeStamp) {
        this.userLastLoginTimeStamp = userLastLoginTimeStamp;
    }

    public Date getUserCreationTimestamp() {
        return userCreationTimestamp;
    }

    public void setUserCreationTimestamp(Date userCreationTimestamp) {
        this.userCreationTimestamp = userCreationTimestamp;
    }

    public Date getUserLastUpdateTimestamp() {
        return userLastUpdateTimestamp;
    }

    public void setUserLastUpdateTimestamp(Date userLastUpdateTimestamp) {
        this.userLastUpdateTimestamp = userLastUpdateTimestamp;
    }

    @XmlTransient
    public Collection<Client> getClientCollection() {
        return clientCollection;
    }

    public void setClientCollection(Collection<Client> clientCollection) {
        this.clientCollection = clientCollection;
    }

    @XmlTransient
    public Collection<Riskanalysis> getRiskanalysisCollection() {
        return riskanalysisCollection;
    }

    public void setRiskanalysisCollection(Collection<Riskanalysis> riskanalysisCollection) {
        this.riskanalysisCollection = riskanalysisCollection;
    }

    @XmlTransient
    public Collection<Traininggroup> getTraininggroupCollection() {
        return traininggroupCollection;
    }

    public void setTraininggroupCollection(Collection<Traininggroup> traininggroupCollection) {
        this.traininggroupCollection = traininggroupCollection;
    }

    @XmlTransient
    public Collection<Incident> getIncidentCollection() {
        return incidentCollection;
    }

    public void setIncidentCollection(Collection<Incident> incidentCollection) {
        this.incidentCollection = incidentCollection;
    }

    public Client getUserClientID() {
        return userClientID;
    }

    public void setUserClientID(Client userClientID) {
        this.userClientID = userClientID;
    }

    public Person getUserPersonID() {
        return userPersonID;
    }

    public void setUserPersonID(Person userPersonID) {
        this.userPersonID = userPersonID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userID != null ? userID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.userID == null && other.userID != null) || (this.userID != null && !this.userID.equals(other.userID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return userPersonID.getDescription();
    }
    
}
