/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskconsequencereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskconsequencereference.findAll", query = "SELECT r FROM Riskconsequencereference r")
    , @NamedQuery(name = "Riskconsequencereference.findByRiskConsequenceReferenceID", query = "SELECT r FROM Riskconsequencereference r WHERE r.riskConsequenceReferenceID = :riskConsequenceReferenceID")})
public class Riskconsequencereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskConsequenceReferenceID")
    private Integer riskConsequenceReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "riskConsequenceReferenceName")
    private String riskConsequenceReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskConsequenceReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;
    @JoinColumn(name = "riskConsequenceRiskReferenceID", referencedColumnName = "riskReferenceID")
    @ManyToOne(optional = false)
    private Riskreference riskConsequenceRiskReferenceID;

    public Riskconsequencereference() {
    }

    public Riskconsequencereference(Integer riskConsequenceReferenceID) {
        this.riskConsequenceReferenceID = riskConsequenceReferenceID;
    }

    public Riskconsequencereference(Integer riskConsequenceReferenceID, String riskConsequenceReferenceName) {
        this.riskConsequenceReferenceID = riskConsequenceReferenceID;
        this.riskConsequenceReferenceName = riskConsequenceReferenceName;
    }

    public Integer getRiskConsequenceReferenceID() {
        return riskConsequenceReferenceID;
    }

    public void setRiskConsequenceReferenceID(Integer riskConsequenceReferenceID) {
        this.riskConsequenceReferenceID = riskConsequenceReferenceID;
    }

    public String getRiskConsequenceReferenceName() {
        return riskConsequenceReferenceName;
    }

    public void setRiskConsequenceReferenceName(String riskConsequenceReferenceName) {
        this.riskConsequenceReferenceName = riskConsequenceReferenceName;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    public Riskreference getRiskConsequenceRiskReferenceID() {
        return riskConsequenceRiskReferenceID;
    }

    public void setRiskConsequenceRiskReferenceID(Riskreference riskConsequenceRiskReferenceID) {
        this.riskConsequenceRiskReferenceID = riskConsequenceRiskReferenceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskConsequenceReferenceID != null ? riskConsequenceReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskconsequencereference)) {
            return false;
        }
        Riskconsequencereference other = (Riskconsequencereference) object;
        if ((this.riskConsequenceReferenceID == null && other.riskConsequenceReferenceID != null) || (this.riskConsequenceReferenceID != null && !this.riskConsequenceReferenceID.equals(other.riskConsequenceReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskconsequencereference[ riskConsequenceReferenceID=" + riskConsequenceReferenceID + " ]";
    }
    
}
