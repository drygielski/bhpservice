/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "insurancetitle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insurancetitle.findAll", query = "SELECT i FROM Insurancetitle i")
    , @NamedQuery(name = "Insurancetitle.findByInsuranceTitleID", query = "SELECT i FROM Insurancetitle i WHERE i.insuranceTitleID = :insuranceTitleID")})
public class Insurancetitle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "insuranceTitleID")
    private Integer insuranceTitleID;
    @JoinColumn(name = "insuranceTitleIncidentID", referencedColumnName = "incidentID")
    @ManyToOne(optional = false)
    private Incident insuranceTitleIncidentID;
    @JoinColumn(name = "insuranceTitleReferenceID", referencedColumnName = "insuranceTitleReferenceID")
    @ManyToOne(optional = false)
    private Insurancetitlereference insuranceTitleReferenceID;

    public Insurancetitle() {
    }

    public Insurancetitle(Integer insuranceTitleID) {
        this.insuranceTitleID = insuranceTitleID;
    }

    public Integer getInsuranceTitleID() {
        return insuranceTitleID;
    }

    public void setInsuranceTitleID(Integer insuranceTitleID) {
        this.insuranceTitleID = insuranceTitleID;
    }

    public Incident getInsuranceTitleIncidentID() {
        return insuranceTitleIncidentID;
    }

    public void setInsuranceTitleIncidentID(Incident insuranceTitleIncidentID) {
        this.insuranceTitleIncidentID = insuranceTitleIncidentID;
    }

    public Insurancetitlereference getInsuranceTitleReferenceID() {
        return insuranceTitleReferenceID;
    }

    public void setInsuranceTitleReferenceID(Insurancetitlereference insuranceTitleReferenceID) {
        this.insuranceTitleReferenceID = insuranceTitleReferenceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insuranceTitleID != null ? insuranceTitleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insurancetitle)) {
            return false;
        }
        Insurancetitle other = (Insurancetitle) object;
        if ((this.insuranceTitleID == null && other.insuranceTitleID != null) || (this.insuranceTitleID != null && !this.insuranceTitleID.equals(other.insuranceTitleID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Insurancetitle[ insuranceTitleID=" + insuranceTitleID + " ]";
    }
    
}
