/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskreference.findAll", query = "SELECT r FROM Riskreference r")
    , @NamedQuery(name = "Riskreference.findByRiskReferenceID", query = "SELECT r FROM Riskreference r WHERE r.riskReferenceID = :riskReferenceID")})
public class Riskreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskReferenceID")
    private Integer riskReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "riskReferenceName")
    private String riskReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskPreventionReferenceRiskReferenceID")
    private Collection<Riskpreventionreference> riskpreventionreferenceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskSourceReferenceRiskReferenceID")
    private Collection<Risksourcereference> risksourcereferenceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskConsequenceRiskReferenceID")
    private Collection<Riskconsequencereference> riskconsequencereferenceCollection;

    public Riskreference() {
    }

    public Riskreference(Integer riskReferenceID) {
        this.riskReferenceID = riskReferenceID;
    }

    public Riskreference(Integer riskReferenceID, String riskReferenceName) {
        this.riskReferenceID = riskReferenceID;
        this.riskReferenceName = riskReferenceName;
    }

    public Integer getRiskReferenceID() {
        return riskReferenceID;
    }

    public void setRiskReferenceID(Integer riskReferenceID) {
        this.riskReferenceID = riskReferenceID;
    }

    public String getRiskReferenceName() {
        return riskReferenceName;
    }

    public void setRiskReferenceName(String riskReferenceName) {
        this.riskReferenceName = riskReferenceName;
    }

    @XmlTransient
    public Collection<Riskpreventionreference> getRiskpreventionreferenceCollection() {
        return riskpreventionreferenceCollection;
    }

    public void setRiskpreventionreferenceCollection(Collection<Riskpreventionreference> riskpreventionreferenceCollection) {
        this.riskpreventionreferenceCollection = riskpreventionreferenceCollection;
    }

    @XmlTransient
    public Collection<Risksourcereference> getRisksourcereferenceCollection() {
        return risksourcereferenceCollection;
    }

    public void setRisksourcereferenceCollection(Collection<Risksourcereference> risksourcereferenceCollection) {
        this.risksourcereferenceCollection = risksourcereferenceCollection;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @XmlTransient
    public Collection<Riskconsequencereference> getRiskconsequencereferenceCollection() {
        return riskconsequencereferenceCollection;
    }

    public void setRiskconsequencereferenceCollection(Collection<Riskconsequencereference> riskconsequencereferenceCollection) {
        this.riskconsequencereferenceCollection = riskconsequencereferenceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskReferenceID != null ? riskReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskreference)) {
            return false;
        }
        Riskreference other = (Riskreference) object;
        if ((this.riskReferenceID == null && other.riskReferenceID != null) || (this.riskReferenceID != null && !this.riskReferenceID.equals(other.riskReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskreference[ riskReferenceID=" + riskReferenceID + " ]";
    }
    
}
