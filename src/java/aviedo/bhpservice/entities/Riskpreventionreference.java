/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskpreventionreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskpreventionreference.findAll", query = "SELECT r FROM Riskpreventionreference r")
    , @NamedQuery(name = "Riskpreventionreference.findByRiskPreventionReferenceID", query = "SELECT r FROM Riskpreventionreference r WHERE r.riskPreventionReferenceID = :riskPreventionReferenceID")})
public class Riskpreventionreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskPreventionReferenceID")
    private Integer riskPreventionReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "riskPreventionReferenceName")
    private String riskPreventionReferenceName;
    @JoinColumn(name = "riskPreventionReferenceRiskReferenceID", referencedColumnName = "riskReferenceID")
    @ManyToOne(optional = false)
    private Riskreference riskPreventionReferenceRiskReferenceID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskPreventionReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;

    public Riskpreventionreference() {
    }

    public Riskpreventionreference(Integer riskPreventionReferenceID) {
        this.riskPreventionReferenceID = riskPreventionReferenceID;
    }

    public Riskpreventionreference(Integer riskPreventionReferenceID, String riskPreventionReferenceName) {
        this.riskPreventionReferenceID = riskPreventionReferenceID;
        this.riskPreventionReferenceName = riskPreventionReferenceName;
    }

    public Integer getRiskPreventionReferenceID() {
        return riskPreventionReferenceID;
    }

    public void setRiskPreventionReferenceID(Integer riskPreventionReferenceID) {
        this.riskPreventionReferenceID = riskPreventionReferenceID;
    }

    public String getRiskPreventionReferenceName() {
        return riskPreventionReferenceName;
    }

    public void setRiskPreventionReferenceName(String riskPreventionReferenceName) {
        this.riskPreventionReferenceName = riskPreventionReferenceName;
    }

    public Riskreference getRiskPreventionReferenceRiskReferenceID() {
        return riskPreventionReferenceRiskReferenceID;
    }

    public void setRiskPreventionReferenceRiskReferenceID(Riskreference riskPreventionReferenceRiskReferenceID) {
        this.riskPreventionReferenceRiskReferenceID = riskPreventionReferenceRiskReferenceID;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskPreventionReferenceID != null ? riskPreventionReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskpreventionreference)) {
            return false;
        }
        Riskpreventionreference other = (Riskpreventionreference) object;
        if ((this.riskPreventionReferenceID == null && other.riskPreventionReferenceID != null) || (this.riskPreventionReferenceID != null && !this.riskPreventionReferenceID.equals(other.riskPreventionReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskpreventionreference[ riskPreventionReferenceID=" + riskPreventionReferenceID + " ]";
    }
    
}
