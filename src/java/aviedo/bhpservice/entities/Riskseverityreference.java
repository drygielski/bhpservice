/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskseverityreference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskseverityreference.findAll", query = "SELECT r FROM Riskseverityreference r")
    , @NamedQuery(name = "Riskseverityreference.findByRiskSeverityReferenceID", query = "SELECT r FROM Riskseverityreference r WHERE r.riskSeverityReferenceID = :riskSeverityReferenceID")
    , @NamedQuery(name = "Riskseverityreference.findByRiskSeverityReferenceName", query = "SELECT r FROM Riskseverityreference r WHERE r.riskSeverityReferenceName = :riskSeverityReferenceName")})
public class Riskseverityreference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskSeverityReferenceID")
    private Short riskSeverityReferenceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "riskSeverityReferenceName")
    private String riskSeverityReferenceName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "riskIdentificationRiskSeverityReferenceID")
    private Collection<Riskidentification> riskidentificationCollection;

    public Riskseverityreference() {
    }

    public Riskseverityreference(Short riskSeverityReferenceID) {
        this.riskSeverityReferenceID = riskSeverityReferenceID;
    }

    public Riskseverityreference(Short riskSeverityReferenceID, String riskSeverityReferenceName) {
        this.riskSeverityReferenceID = riskSeverityReferenceID;
        this.riskSeverityReferenceName = riskSeverityReferenceName;
    }

    public Short getRiskSeverityReferenceID() {
        return riskSeverityReferenceID;
    }

    public void setRiskSeverityReferenceID(Short riskSeverityReferenceID) {
        this.riskSeverityReferenceID = riskSeverityReferenceID;
    }

    public String getRiskSeverityReferenceName() {
        return riskSeverityReferenceName;
    }

    public void setRiskSeverityReferenceName(String riskSeverityReferenceName) {
        this.riskSeverityReferenceName = riskSeverityReferenceName;
    }

    @XmlTransient
    public Collection<Riskidentification> getRiskidentificationCollection() {
        return riskidentificationCollection;
    }

    public void setRiskidentificationCollection(Collection<Riskidentification> riskidentificationCollection) {
        this.riskidentificationCollection = riskidentificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskSeverityReferenceID != null ? riskSeverityReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskseverityreference)) {
            return false;
        }
        Riskseverityreference other = (Riskseverityreference) object;
        if ((this.riskSeverityReferenceID == null && other.riskSeverityReferenceID != null) || (this.riskSeverityReferenceID != null && !this.riskSeverityReferenceID.equals(other.riskSeverityReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskseverityreference[ riskSeverityReferenceID=" + riskSeverityReferenceID + " ]";
    }
    
}
