/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "workplacereference")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Workplacereference.findAll", query = "SELECT w FROM Workplacereference w")
    , @NamedQuery(name = "Workplacereference.findByWorkplaceReferenceID", query = "SELECT w FROM Workplacereference w WHERE w.workplaceReferenceID = :workplaceReferenceID")
    , @NamedQuery(name = "Workplacereference.findByWorkplaceReferenceDescription", query = "SELECT w FROM Workplacereference w WHERE w.workplaceReferenceDescription = :workplaceReferenceDescription")})
public class Workplacereference implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "workplaceReferenceID")
    private Integer workplaceReferenceID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "workplaceReferenceName")
    private String workplaceReferenceName;
    @Size(max = 45)
    @Column(name = "workplaceReferenceDescription")
    private String workplaceReferenceDescription;
    @OneToMany(mappedBy = "workplaceReferenceID")
    private Collection<Workplace> workplaceCollection;

    public Workplacereference() {
    }

    public Workplacereference(Integer workplaceReferenceID) {
        this.workplaceReferenceID = workplaceReferenceID;
    }

    public Workplacereference(Integer workplaceReferenceID, String workplaceReferenceName) {
        this.workplaceReferenceID = workplaceReferenceID;
        this.workplaceReferenceName = workplaceReferenceName;
    }

    public Integer getWorkplaceReferenceID() {
        return workplaceReferenceID;
    }

    public void setWorkplaceReferenceID(Integer workplaceReferenceID) {
        this.workplaceReferenceID = workplaceReferenceID;
    }

    public String getWorkplaceReferenceName() {
        return workplaceReferenceName;
    }

    public void setWorkplaceReferenceName(String workplaceReferenceName) {
        this.workplaceReferenceName = workplaceReferenceName;
    }

    public String getWorkplaceReferenceDescription() {
        return workplaceReferenceDescription;
    }

    public void setWorkplaceReferenceDescription(String workplaceReferenceDescription) {
        this.workplaceReferenceDescription = workplaceReferenceDescription;
    }

    @XmlTransient
    public Collection<Workplace> getWorkplaceCollection() {
        return workplaceCollection;
    }

    public void setWorkplaceCollection(Collection<Workplace> workplaceCollection) {
        this.workplaceCollection = workplaceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workplaceReferenceID != null ? workplaceReferenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Workplacereference)) {
            return false;
        }
        Workplacereference other = (Workplacereference) object;
        if ((this.workplaceReferenceID == null && other.workplaceReferenceID != null) || (this.workplaceReferenceID != null && !this.workplaceReferenceID.equals(other.workplaceReferenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return workplaceReferenceDescription;
    }
    
}
