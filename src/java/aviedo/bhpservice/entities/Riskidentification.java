/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aviedo.bhpservice.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcin
 */
@Entity
@Table(name = "riskidentification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Riskidentification.findAll", query = "SELECT r FROM Riskidentification r")
    , @NamedQuery(name = "Riskidentification.findByRiskIdentificationID", query = "SELECT r FROM Riskidentification r WHERE r.riskIdentificationID = :riskIdentificationID")})
public class Riskidentification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "riskIdentificationID")
    private Integer riskIdentificationID;
    @JoinColumn(name = "riskIdentificationRiskConsequenceReferenceID", referencedColumnName = "riskConsequenceReferenceID")
    @ManyToOne(optional = false)
    private Riskconsequencereference riskIdentificationRiskConsequenceReferenceID;
    @JoinColumn(name = "riskIdentificationRiskPreventionReferenceID", referencedColumnName = "riskPreventionReferenceID")
    @ManyToOne(optional = false)
    private Riskpreventionreference riskIdentificationRiskPreventionReferenceID;
    @JoinColumn(name = "riskIdentificationRiskReferenceID", referencedColumnName = "riskReferenceID")
    @ManyToOne(optional = false)
    private Riskreference riskIdentificationRiskReferenceID;
    @JoinColumn(name = "riskIdentificationRiskSourceReferenceID", referencedColumnName = "riskSourceReferenceID")
    @ManyToOne(optional = false)
    private Risksourcereference riskIdentificationRiskSourceReferenceID;
    @JoinColumn(name = "riskIdentificationRiskAnalysisID", referencedColumnName = "riskAnalysisID")
    @ManyToOne(optional = false)
    private Riskanalysis riskIdentificationRiskAnalysisID;
    @JoinColumn(name = "riskIdentificationRiskProbabilityReferenceID", referencedColumnName = "riskProbabilityReferenceID")
    @ManyToOne(optional = false)
    private Riskprobabilityreference riskIdentificationRiskProbabilityReferenceID;
    @JoinColumn(name = "riskIdentificationRiskSeverityReferenceID", referencedColumnName = "riskSeverityReferenceID")
    @ManyToOne(optional = false)
    private Riskseverityreference riskIdentificationRiskSeverityReferenceID;
    @JoinColumn(name = "riskIdentificationRisksAssessmentReferenceID", referencedColumnName = "risksAssessmentReferenceID")
    @ManyToOne(optional = false)
    private Risksassessmentreference riskIdentificationRisksAssessmentReferenceID;

    public Riskidentification() {
    }

    public Riskidentification(Integer riskIdentificationID) {
        this.riskIdentificationID = riskIdentificationID;
    }

    public Integer getRiskIdentificationID() {
        return riskIdentificationID;
    }

    public void setRiskIdentificationID(Integer riskIdentificationID) {
        this.riskIdentificationID = riskIdentificationID;
    }

    public Riskconsequencereference getRiskIdentificationRiskConsequenceReferenceID() {
        return riskIdentificationRiskConsequenceReferenceID;
    }

    public void setRiskIdentificationRiskConsequenceReferenceID(Riskconsequencereference riskIdentificationRiskConsequenceReferenceID) {
        this.riskIdentificationRiskConsequenceReferenceID = riskIdentificationRiskConsequenceReferenceID;
    }

    public Riskpreventionreference getRiskIdentificationRiskPreventionReferenceID() {
        return riskIdentificationRiskPreventionReferenceID;
    }

    public void setRiskIdentificationRiskPreventionReferenceID(Riskpreventionreference riskIdentificationRiskPreventionReferenceID) {
        this.riskIdentificationRiskPreventionReferenceID = riskIdentificationRiskPreventionReferenceID;
    }

    public Riskreference getRiskIdentificationRiskReferenceID() {
        return riskIdentificationRiskReferenceID;
    }

    public void setRiskIdentificationRiskReferenceID(Riskreference riskIdentificationRiskReferenceID) {
        this.riskIdentificationRiskReferenceID = riskIdentificationRiskReferenceID;
    }

    public Risksourcereference getRiskIdentificationRiskSourceReferenceID() {
        return riskIdentificationRiskSourceReferenceID;
    }

    public void setRiskIdentificationRiskSourceReferenceID(Risksourcereference riskIdentificationRiskSourceReferenceID) {
        this.riskIdentificationRiskSourceReferenceID = riskIdentificationRiskSourceReferenceID;
    }

    public Riskanalysis getRiskIdentificationRiskAnalysisID() {
        return riskIdentificationRiskAnalysisID;
    }

    public void setRiskIdentificationRiskAnalysisID(Riskanalysis riskIdentificationRiskAnalysisID) {
        this.riskIdentificationRiskAnalysisID = riskIdentificationRiskAnalysisID;
    }

    public Riskprobabilityreference getRiskIdentificationRiskProbabilityReferenceID() {
        return riskIdentificationRiskProbabilityReferenceID;
    }

    public void setRiskIdentificationRiskProbabilityReferenceID(Riskprobabilityreference riskIdentificationRiskProbabilityReferenceID) {
        this.riskIdentificationRiskProbabilityReferenceID = riskIdentificationRiskProbabilityReferenceID;
    }

    public Riskseverityreference getRiskIdentificationRiskSeverityReferenceID() {
        return riskIdentificationRiskSeverityReferenceID;
    }

    public void setRiskIdentificationRiskSeverityReferenceID(Riskseverityreference riskIdentificationRiskSeverityReferenceID) {
        this.riskIdentificationRiskSeverityReferenceID = riskIdentificationRiskSeverityReferenceID;
    }

    public Risksassessmentreference getRiskIdentificationRisksAssessmentReferenceID() {
        return riskIdentificationRisksAssessmentReferenceID;
    }

    public void setRiskIdentificationRisksAssessmentReferenceID(Risksassessmentreference riskIdentificationRisksAssessmentReferenceID) {
        this.riskIdentificationRisksAssessmentReferenceID = riskIdentificationRisksAssessmentReferenceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (riskIdentificationID != null ? riskIdentificationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Riskidentification)) {
            return false;
        }
        Riskidentification other = (Riskidentification) object;
        if ((this.riskIdentificationID == null && other.riskIdentificationID != null) || (this.riskIdentificationID != null && !this.riskIdentificationID.equals(other.riskIdentificationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aviedo.bhpservice.entities.Riskidentification[ riskIdentificationID=" + riskIdentificationID + " ]";
    }
    
}
