package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Victimtestimony;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.VictimtestimonyFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("victimtestimonyController")
@SessionScoped
public class VictimtestimonyController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.VictimtestimonyFacade ejbFacade;
    private List<Victimtestimony> items = null;
    private Victimtestimony selected;

    @Inject
    private IncidentController incidentControl;
    
    public VictimtestimonyController() {
    }

    public Victimtestimony getSelected() {
        return selected;
    }

    public void setSelected(Victimtestimony selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private VictimtestimonyFacade getFacade() {
        return ejbFacade;
    }

    public Victimtestimony prepareCreate() {
        selected = new Victimtestimony();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setVictimTestimonyIncidentID(incidentControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("VictimtestimonyCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("VictimtestimonyUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("VictimtestimonyDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Victimtestimony> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Victimtestimony> getIncidentItems() {
        getItems();
        //Filter trainings
        if(incidentControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getVictimTestimonyIncidentID().getIncidentID(), incidentControl.getSelected().getIncidentID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    
    public Victimtestimony getVictimtestimony(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Victimtestimony> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Victimtestimony> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Victimtestimony.class)
    public static class VictimtestimonyControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            VictimtestimonyController controller = (VictimtestimonyController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "victimtestimonyController");
            return controller.getVictimtestimony(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Victimtestimony) {
                Victimtestimony o = (Victimtestimony) object;
                return getStringKey(o.getVictimTestimonyID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Victimtestimony.class.getName()});
                return null;
            }
        }

    }

}
