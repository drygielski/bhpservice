package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Positionduty;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.PositiondutyFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("positiondutyController")
@SessionScoped
public class PositiondutyController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.PositiondutyFacade ejbFacade;
    private List<Positionduty> items = null;
    private Positionduty selected;
    
    @Inject
    private PositionController positionControl;

    public PositiondutyController() {
    }

    public Positionduty getSelected() {
        return selected;
    }

    public void setSelected(Positionduty selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PositiondutyFacade getFacade() {
        return ejbFacade;
    }

    public Positionduty prepareCreate() {
        selected = new Positionduty();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setPositionDutyPositionID(positionControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PositiondutyCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PositiondutyUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PositiondutyDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Positionduty> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Positionduty> getPostionItems() {
        getItems();
        //Filter
        if(positionControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getPositionDutyPositionID().getPositionID(), positionControl.getSelected().getPositionID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    public Positionduty getPositionduty(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Positionduty> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Positionduty> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Positionduty.class)
    public static class PositiondutyControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PositiondutyController controller = (PositiondutyController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "positiondutyController");
            return controller.getPositionduty(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Positionduty) {
                Positionduty o = (Positionduty) object;
                return getStringKey(o.getPositionDutyID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Positionduty.class.getName()});
                return null;
            }
        }

    }

}
