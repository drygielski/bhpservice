package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Incidentcause;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.IncidentcauseFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("incidentcauseController")
@SessionScoped
public class IncidentcauseController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.IncidentcauseFacade ejbFacade;
    private List<Incidentcause> items = null;
    private Incidentcause selected;

    @Inject
    private IncidentController incidentControl;
    
    public IncidentcauseController() {
    }

    public Incidentcause getSelected() {
        return selected;
    }

    public void setSelected(Incidentcause selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private IncidentcauseFacade getFacade() {
        return ejbFacade;
    }

    public Incidentcause prepareCreate() {
        selected = new Incidentcause();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setIncidentCauseIncidentID(incidentControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("IncidentcauseCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("IncidentcauseUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("IncidentcauseDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Incidentcause> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }
    
    public List<Incidentcause> getIncidentItems() {
        getItems();
        //Filter trainings
        if(incidentControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getIncidentCauseIncidentID().getIncidentID(), incidentControl.getSelected().getIncidentID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }

    public Incidentcause getIncidentcause(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Incidentcause> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Incidentcause> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Incidentcause.class)
    public static class IncidentcauseControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            IncidentcauseController controller = (IncidentcauseController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "incidentcauseController");
            return controller.getIncidentcause(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Incidentcause) {
                Incidentcause o = (Incidentcause) object;
                return getStringKey(o.getIncidentCauseID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Incidentcause.class.getName()});
                return null;
            }
        }

    }

}
