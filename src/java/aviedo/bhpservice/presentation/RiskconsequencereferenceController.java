package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Riskconsequencereference;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.RiskconsequencereferenceFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("riskconsequencereferenceController")
@SessionScoped
public class RiskconsequencereferenceController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.RiskconsequencereferenceFacade ejbFacade;
    private List<Riskconsequencereference> items = null;
    private Riskconsequencereference selected;

    @Inject
    private RiskidentificationController riskIdentificationControl;
    
    public RiskconsequencereferenceController() {
    }

    public Riskconsequencereference getSelected() {
        return selected;
    }

    public void setSelected(Riskconsequencereference selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private RiskconsequencereferenceFacade getFacade() {
        return ejbFacade;
    }

    public Riskconsequencereference prepareCreate() {
        selected = new Riskconsequencereference();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("RiskconsequencereferenceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("RiskconsequencereferenceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("RiskconsequencereferenceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Riskconsequencereference> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Riskconsequencereference getRiskconsequencereference(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Riskconsequencereference> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Riskconsequencereference> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }
    
    public List<Riskconsequencereference> getRiskIdentificationItems() {
        getItems();
        //Filter
        if(riskIdentificationControl.getSelected().getRiskIdentificationRiskReferenceID() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getRiskConsequenceRiskReferenceID().getRiskReferenceID(), riskIdentificationControl.getSelected().getRiskIdentificationRiskReferenceID().getRiskReferenceID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }

    @FacesConverter(forClass = Riskconsequencereference.class)
    public static class RiskconsequencereferenceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RiskconsequencereferenceController controller = (RiskconsequencereferenceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "riskconsequencereferenceController");
            return controller.getRiskconsequencereference(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Riskconsequencereference) {
                Riskconsequencereference o = (Riskconsequencereference) object;
                return getStringKey(o.getRiskConsequenceReferenceID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Riskconsequencereference.class.getName()});
                return null;
            }
        }

    }

}
