package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Training;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.TrainingFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("trainingController")
@SessionScoped
public class TrainingController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.TrainingFacade ejbFacade;
    private List<Training> items = null;
    private Training selected;
    
    @Inject
    private TraininggroupController trainingGroupControl;
    @Inject
    private EmployeeController employeeControl;
    
    public TrainingController() {
    }
    public Training getSelected() {
        return selected;
    }
    public void setSelected(Training selected) {
        this.selected = selected;
    }
    protected void setEmbeddableKeys() {
    }
    protected void initializeEmbeddableKey() {
    }
    private TrainingFacade getFacade() {
        return ejbFacade;
    }
    public Training prepareCreate() {
        selected = new Training();
        initializeEmbeddableKey();
        return selected;
    }
    public void create() {
        selected.setTrainingGroupTrainingGroupID(trainingGroupControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("TrainingCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }
    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("TrainingUpdated"));
    }
    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("TrainingDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }
    public List<Training> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }
    public List<Training> getTrainingItems() {
        getItems();
        //Filter trainings
        if(trainingGroupControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getTrainingGroupTrainingGroupID().getTrainingGroupID(), trainingGroupControl.getSelected().getTrainingGroupID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    private void persist(PersistAction persistAction, 
            String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    selected = getFacade().update(selected);
                    employeeControl.setSelected(selected.getTrainingEmployeeID());
                    if(trainingGroupControl.getSelected().getTrainingInitial()) {
                        employeeControl.getSelected().setEmployeeInitialTrainingID(selected);
                    }else{
                        employeeControl.getSelected().setEmployeePeriodicTrainingID(selected);
                    }
                    employeeControl.updateTrainingInfo();
                } else {
                    employeeControl.setSelected(selected.getTrainingEmployeeID());
                    if(trainingGroupControl.getSelected().getTrainingInitial()) {
                        employeeControl.getSelected().setEmployeeInitialTrainingID(null);
                    }else{
                        employeeControl.getSelected().setEmployeePeriodicTrainingID(null);
                    }
                    employeeControl.updateTrainingInfo();
                    getFacade().remove(selected);
                    
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }
    public Training getTraining(java.lang.Integer id) {
        return getFacade().find(id);
    }
    public List<Training> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }
    public List<Training> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }
    @FacesConverter(forClass = Training.class)
    public static class TrainingControllerConverter 
            implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrainingController controller = (TrainingController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trainingController");
            return controller.getTraining(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Training) {
                Training o = (Training) object;
                return getStringKey(o.getTrainingID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Training.class.getName()});
                return null;
            }
        }

    }
}
