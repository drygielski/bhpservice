package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Riskidentification;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.RiskidentificationFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("riskidentificationController")
@SessionScoped
public class RiskidentificationController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.RiskidentificationFacade ejbFacade;
    private List<Riskidentification> items = null;
    private Riskidentification selected;

    @Inject
    private RiskanalysisController riskAnalysisControl;
    
    public RiskidentificationController() {
    }

    public Riskidentification getSelected() {
        return selected;
    }

    public void setSelected(Riskidentification selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private RiskidentificationFacade getFacade() {
        return ejbFacade;
    }

    public Riskidentification prepareCreate() {
        selected = new Riskidentification();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setRiskIdentificationRiskAnalysisID(riskAnalysisControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("RiskidentificationCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("RiskidentificationUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("RiskidentificationDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Riskidentification> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }
    
    public List<Riskidentification> getRiskAnalysisItems() {
        getItems();
        //Filter
        if(riskAnalysisControl.getSelected() != null){
            return items.stream().filter(a -> Objects.equals(a.getRiskIdentificationRiskAnalysisID().getRiskAnalysisID(), 
                    riskAnalysisControl.getSelected().getRiskAnalysisID())).collect(Collectors.toList());
        }
        return null;
    }

    public Riskidentification getRiskidentification(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Riskidentification> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Riskidentification> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Riskidentification.class)
    public static class RiskidentificationControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RiskidentificationController controller = (RiskidentificationController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "riskidentificationController");
            return controller.getRiskidentification(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Riskidentification) {
                Riskidentification o = (Riskidentification) object;
                return getStringKey(o.getRiskIdentificationID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Riskidentification.class.getName()});
                return null;
            }
        }

    }

}
