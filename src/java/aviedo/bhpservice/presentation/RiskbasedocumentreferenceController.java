package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Riskbasedocumentreference;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.RiskbasedocumentreferenceFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("riskbasedocumentreferenceController")
@SessionScoped
public class RiskbasedocumentreferenceController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.RiskbasedocumentreferenceFacade ejbFacade;
    private List<Riskbasedocumentreference> items = null;
    private Riskbasedocumentreference selected;
    
    public RiskbasedocumentreferenceController() {
    }

    public Riskbasedocumentreference getSelected() {
        return selected;
    }

    public void setSelected(Riskbasedocumentreference selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private RiskbasedocumentreferenceFacade getFacade() {
        return ejbFacade;
    }

    public Riskbasedocumentreference prepareCreate() {
        selected = new Riskbasedocumentreference();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("RiskbasedocumentreferenceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("RiskbasedocumentreferenceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("RiskbasedocumentreferenceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Riskbasedocumentreference> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Riskbasedocumentreference getRiskbasedocumentreference(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Riskbasedocumentreference> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Riskbasedocumentreference> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Riskbasedocumentreference.class)
    public static class RiskbasedocumentreferenceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RiskbasedocumentreferenceController controller = (RiskbasedocumentreferenceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "riskbasedocumentreferenceController");
            return controller.getRiskbasedocumentreference(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Riskbasedocumentreference) {
                Riskbasedocumentreference o = (Riskbasedocumentreference) object;
                return getStringKey(o.getRiskBaseDocumentReferenceID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Riskbasedocumentreference.class.getName()});
                return null;
            }
        }

    }

}
