package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Risksourcereference;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.RisksourcereferenceFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("risksourcereferenceController")
@SessionScoped
public class RisksourcereferenceController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.RisksourcereferenceFacade ejbFacade;
    private List<Risksourcereference> items = null;
    private Risksourcereference selected;

    @Inject
    private RiskidentificationController riskIdentificationControl;
    
    public RisksourcereferenceController() {
    }

    public Risksourcereference getSelected() {
        return selected;
    }

    public void setSelected(Risksourcereference selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private RisksourcereferenceFacade getFacade() {
        return ejbFacade;
    }

    public Risksourcereference prepareCreate() {
        selected = new Risksourcereference();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("RisksourcereferenceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("RisksourcereferenceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("RisksourcereferenceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Risksourcereference> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Risksourcereference getRisksourcereference(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Risksourcereference> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Risksourcereference> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }
    
    public List<Risksourcereference> getRiskIdentificationItems() {
        getItems();
        //Filter
        if(riskIdentificationControl.getSelected().getRiskIdentificationRiskReferenceID() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getRiskSourceReferenceRiskReferenceID().getRiskReferenceID(), riskIdentificationControl.getSelected().getRiskIdentificationRiskReferenceID().getRiskReferenceID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }

    @FacesConverter(forClass = Risksourcereference.class)
    public static class RisksourcereferenceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RisksourcereferenceController controller = (RisksourcereferenceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "risksourcereferenceController");
            return controller.getRisksourcereference(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Risksourcereference) {
                Risksourcereference o = (Risksourcereference) object;
                return getStringKey(o.getRiskSourceReferenceID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Risksourcereference.class.getName()});
                return null;
            }
        }

    }

}
