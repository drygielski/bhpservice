package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Proffesionclassificationcode;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.ProffesionclassificationcodeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("proffesionclassificationcodeController")
@SessionScoped
public class ProffesionclassificationcodeController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.ProffesionclassificationcodeFacade ejbFacade;
    private List<Proffesionclassificationcode> items = null;
    private Proffesionclassificationcode selected;

    public ProffesionclassificationcodeController() {
    }

    public Proffesionclassificationcode getSelected() {
        return selected;
    }

    public void setSelected(Proffesionclassificationcode selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ProffesionclassificationcodeFacade getFacade() {
        return ejbFacade;
    }

    public Proffesionclassificationcode prepareCreate() {
        selected = new Proffesionclassificationcode();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ProffesionclassificationcodeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ProffesionclassificationcodeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ProffesionclassificationcodeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Proffesionclassificationcode> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Proffesionclassificationcode getProffesionclassificationcode(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Proffesionclassificationcode> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Proffesionclassificationcode> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Proffesionclassificationcode.class)
    public static class ProffesionclassificationcodeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProffesionclassificationcodeController controller = (ProffesionclassificationcodeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "proffesionclassificationcodeController");
            return controller.getProffesionclassificationcode(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Proffesionclassificationcode) {
                Proffesionclassificationcode o = (Proffesionclassificationcode) object;
                return getStringKey(o.getProffesionClassificationCodeID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Proffesionclassificationcode.class.getName()});
                return null;
            }
        }

    }

}
