package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Incidentinjury;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.IncidentinjuryFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("incidentinjuryController")
@SessionScoped
public class IncidentinjuryController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.IncidentinjuryFacade ejbFacade;
    private List<Incidentinjury> items = null;
    private Incidentinjury selected;

    @Inject
    private IncidentController incidentControl;
    
    public IncidentinjuryController() {
    }

    public Incidentinjury getSelected() {
        return selected;
    }

    public void setSelected(Incidentinjury selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private IncidentinjuryFacade getFacade() {
        return ejbFacade;
    }

    public Incidentinjury prepareCreate() {
        selected = new Incidentinjury();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setIncidentInjuryIncidentID(incidentControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("IncidentinjuryCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("IncidentinjuryUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("IncidentinjuryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Incidentinjury> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Incidentinjury> getIncidentItems() {
        getItems();
        //Filter trainings
        if(incidentControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getIncidentInjuryIncidentID().getIncidentID(), incidentControl.getSelected().getIncidentID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    
    public Incidentinjury getIncidentinjury(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Incidentinjury> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Incidentinjury> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Incidentinjury.class)
    public static class IncidentinjuryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            IncidentinjuryController controller = (IncidentinjuryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "incidentinjuryController");
            return controller.getIncidentinjury(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Incidentinjury) {
                Incidentinjury o = (Incidentinjury) object;
                return getStringKey(o.getIncidentInjuryID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Incidentinjury.class.getName()});
                return null;
            }
        }

    }

}
