package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Witnesstestimony;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.WitnesstestimonyFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("witnesstestimonyController")
@SessionScoped
public class WitnesstestimonyController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.WitnesstestimonyFacade ejbFacade;
    private List<Witnesstestimony> items = null;
    private Witnesstestimony selected;

    @Inject
    private IncidentController incidentControl;
    
    public WitnesstestimonyController() {
    }

    public Witnesstestimony getSelected() {
        return selected;
    }

    public void setSelected(Witnesstestimony selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private WitnesstestimonyFacade getFacade() {
        return ejbFacade;
    }

    public Witnesstestimony prepareCreate() {
        selected = new Witnesstestimony();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setWitnessTestimonyIncidentID(incidentControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("WitnesstestimonyCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("WitnesstestimonyUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("WitnesstestimonyDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Witnesstestimony> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Witnesstestimony> getIncidentItems() {
        getItems();
        //Filter trainings
        if(incidentControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getWitnessTestimonyIncidentID().getIncidentID(), incidentControl.getSelected().getIncidentID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    
    public Witnesstestimony getWitnesstestimony(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Witnesstestimony> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Witnesstestimony> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Witnesstestimony.class)
    public static class WitnesstestimonyControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            WitnesstestimonyController controller = (WitnesstestimonyController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "witnesstestimonyController");
            return controller.getWitnesstestimony(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Witnesstestimony) {
                Witnesstestimony o = (Witnesstestimony) object;
                return getStringKey(o.getWinessTestimonyID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Witnesstestimony.class.getName()});
                return null;
            }
        }

    }

}
