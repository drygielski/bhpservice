package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Positionqualification;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.PositionqualificationFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("positionqualificationController")
@SessionScoped
public class PositionqualificationController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.PositionqualificationFacade ejbFacade;
    private List<Positionqualification> items = null;
    private Positionqualification selected;

    @Inject
    private PositionController positionControl;
    
    public PositionqualificationController() {
    }

    public Positionqualification getSelected() {
        return selected;
    }

    public void setSelected(Positionqualification selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PositionqualificationFacade getFacade() {
        return ejbFacade;
    }

    public Positionqualification prepareCreate() {
        selected = new Positionqualification();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setPositionQualificationPositionID(positionControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PositionqualificationCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PositionqualificationUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PositionqualificationDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Positionqualification> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }
    public List<Positionqualification> getPositionItems() {
        getItems();
        //Filter Ecucational activities
        if(positionControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getPositionQualificationPositionID().getPositionID(), positionControl.getSelected().getPositionID()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public Positionqualification getPositionqualification(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Positionqualification> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Positionqualification> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Positionqualification.class)
    public static class PositionqualificationControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PositionqualificationController controller = (PositionqualificationController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "positionqualificationController");
            return controller.getPositionqualification(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Positionqualification) {
                Positionqualification o = (Positionqualification) object;
                return getStringKey(o.getPositionQualificationID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Positionqualification.class.getName()});
                return null;
            }
        }

    }

}
