package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Incidentcausereference;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.IncidentcausereferenceFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("incidentcausereferenceController")
@SessionScoped
public class IncidentcausereferenceController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.IncidentcausereferenceFacade ejbFacade;
    private List<Incidentcausereference> items = null;
    private Incidentcausereference selected;

    public IncidentcausereferenceController() {
    }

    public Incidentcausereference getSelected() {
        return selected;
    }

    public void setSelected(Incidentcausereference selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private IncidentcausereferenceFacade getFacade() {
        return ejbFacade;
    }

    public Incidentcausereference prepareCreate() {
        selected = new Incidentcausereference();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("IncidentcausereferenceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("IncidentcausereferenceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("IncidentcausereferenceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Incidentcausereference> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Incidentcausereference getIncidentcausereference(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Incidentcausereference> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Incidentcausereference> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Incidentcausereference.class)
    public static class IncidentcausereferenceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            IncidentcausereferenceController controller = (IncidentcausereferenceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "incidentcausereferenceController");
            return controller.getIncidentcausereference(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Incidentcausereference) {
                Incidentcausereference o = (Incidentcausereference) object;
                return getStringKey(o.getIncidentCauseReferenceID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Incidentcausereference.class.getName()});
                return null;
            }
        }

    }

}
