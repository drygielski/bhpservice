package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Employee;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.EmployeeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("employeeController")
@SessionScoped
public class EmployeeController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.EmployeeFacade ejbFacade;
    private List<Employee> items = null;
    private Employee selected;
    
    @Inject
    private CompanyController companyControl;
    @Inject
    private PersondetailsController personDetails;
    @Inject
    private PersonController person;
    @Inject
    private AddressController address;

    public PersonController getPerson() {
        return person;
    }

    public void setPerson(PersonController person) {
        this.person = person;
    }
    
    public PersondetailsController getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(PersondetailsController personDetails) {
        this.personDetails = personDetails;
    }

    public EmployeeController() {
    }

    public Employee getSelected() {
        return selected;
    }

    public void setSelected(Employee selected) {
        this.selected = selected;
        try {
            person.setSelected(selected.getEmployeePersonID());
            address.setSelected(person.getSelected().getPersonAddressID());
        }catch(Exception e) {
            System.out.println("setSelector Exception " + e.toString()); 
        }
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EmployeeFacade getFacade() {
        return ejbFacade;
    }

    public Employee prepareCreate() {
        System.out.println("Employee prepareCreate()");
        selected = new Employee();
        initializeEmbeddableKey();
        selected.setEmployeeCompanyID(companyControl.getSelected());
        System.out.println("Company ID: " + selected.getEmployeeCompanyID());
        selected.setEmployeeEmployeer(false);
//        create();
        person.prepareCreate();
        selected.setEmployeePersonID(person.getSelected());
        return selected;
    }
    
    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        if(personDetails.getSelected() != null) {
            personDetails.update();
        }
        person.update();
        address.update();
        selected.setEmployeeCompanyID(companyControl.getSelected());
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }
    
    public void updateTrainingInfo() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
    }

    public void destroy() {
        personDetails.getPersondetailsByPersonID(selected.getEmployeePersonID().getPersonID());
        if(personDetails.getSelected() != null) {
            personDetails.destroy();
        }
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("EmployeeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Employee> getItems() {
        try {
            items = getFacade().findCompanyID(companyControl.getSelected());
        }catch(NullPointerException e) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Employee> getCompanyItems() {
        getItems();
        //Filter
        if(companyControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getEmployeeCompanyID().getCompanyID(), companyControl.getSelected().getCompanyID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    public Employee getEmployee(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Employee> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Employee> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Employee.class)
    public static class EmployeeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EmployeeController controller = (EmployeeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "employeeController");
            return controller.getEmployee(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Employee) {
                Employee o = (Employee) object;
                return getStringKey(o.getEmployeeID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Employee.class.getName()});
                return null;
            }
        }

    }

}
