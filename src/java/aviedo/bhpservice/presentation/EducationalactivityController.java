package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Educationalactivity;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.EducationalactivityFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("educationalactivityController")
@SessionScoped
public class EducationalactivityController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.EducationalactivityFacade ejbFacade;
    private List<Educationalactivity> items = null;
    private Educationalactivity selected;

    
    @Inject
    private TraininggroupController trainingGroupControl;
    
    public EducationalactivityController() {
    }

    public Educationalactivity getSelected() {
        return selected;
    }

    public void setSelected(Educationalactivity selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EducationalactivityFacade getFacade() {
        return ejbFacade;
    }

    public Educationalactivity prepareCreate() {
        selected = new Educationalactivity();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setEducationalActivityTrainingGroupID(trainingGroupControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("EducationalactivityCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EducationalactivityUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("EducationalactivityDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Educationalactivity> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }
    public List<Educationalactivity> getTrainingItems() {
        getItems();
        //Filter Ecucational activities
        if(trainingGroupControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getEducationalActivityTrainingGroupID().getTrainingGroupID(), trainingGroupControl.getSelected().getTrainingGroupID()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Educationalactivity getEducationalactivity(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Educationalactivity> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Educationalactivity> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Educationalactivity.class)
    public static class EducationalactivityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EducationalactivityController controller = (EducationalactivityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "educationalactivityController");
            return controller.getEducationalactivity(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Educationalactivity) {
                Educationalactivity o = (Educationalactivity) object;
                return getStringKey(o.getEducationalActivityID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Educationalactivity.class.getName()});
                return null;
            }
        }

    }

}
