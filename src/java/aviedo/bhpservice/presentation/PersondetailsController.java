package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Person;
import aviedo.bhpservice.entities.Persondetails;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.PersondetailsFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("persondetailsController")
@SessionScoped
public class PersondetailsController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.PersondetailsFacade ejbFacade;
    private List<Persondetails> items = null;
    private Persondetails selected;

    public PersondetailsController() {
    }

    public Persondetails getSelected() {
        return selected;
    }

    public void setSelected(Persondetails selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PersondetailsFacade getFacade() {
        return ejbFacade;
    }

    public Persondetails prepareCreate() {
        System.out.println("Persondetails prepareCreate()");
        selected = new Persondetails();
        initializeEmbeddableKey();
        return selected;
    }
    
//    public void setSelectedPersonID(Integer id) {
//        List<Persondetails> details = getItems();
//        System.out.println("details size " + details.size());
//        this.selected = null;
//        for(Persondetails personDet:details) {
//            if(personDet.getPersonDetailsPersonID().getPersonID().equals(id)) {
//                System.out.println(personDet.getPersonDetailsID() + "email " + personDet.getPersonDetailsEmail());
//                this.selected = personDet;
//            }
//        }
//    }
    
    public Persondetails createForPerson(Person per) {
        System.out.print("Persondetails createForPerson()" + per.getDescription());
        prepareCreate();
        selected.setPersonDetailsPersonID(per);
        create();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PersondetailsCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        System.out.println("Updating person Details " + selected.toString());
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PersondetailsUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PersondetailsDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Persondetails> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Persondetails getPersondetails(java.lang.Integer id) {
        if(id != null) {
            return getFacade().find(id);
        }else{
            return null;
        }
    }
    public void getPersondetailsByPersonID(java.lang.Integer id) {
        List<Persondetails> details = getItems();
        Persondetails temp;
        this.selected = null;
        for(Persondetails personDet:details) {
            if(personDet.getPersonDetailsPersonID().getPersonID().equals(id)) {
                temp = getFacade().find(personDet.getPersonDetailsID());
                if(temp != null) {
                    this.selected = temp;
                    return;
                }
            }
        }
    }

    public List<Persondetails> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Persondetails> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Persondetails.class)
    public static class PersondetailsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            System.out.println("Getting as object");
            if (value == null || value.length() == 0) {
                return null;
            }
            PersondetailsController controller = (PersondetailsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "persondetailsController");
            return controller.getPersondetails(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Persondetails) {
                Persondetails o = (Persondetails) object;
                return getStringKey(o.getPersonDetailsID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Persondetails.class.getName()});
                return null;
            }
        }

    }

}
