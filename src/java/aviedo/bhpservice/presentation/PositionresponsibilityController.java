package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Positionresponsibility;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.PositionresponsibilityFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("positionresponsibilityController")
@SessionScoped
public class PositionresponsibilityController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.PositionresponsibilityFacade ejbFacade;
    private List<Positionresponsibility> items = null;
    private Positionresponsibility selected;

    @Inject
    private PositionController positionControl;
    
    public PositionresponsibilityController() {
    }

    public Positionresponsibility getSelected() {
        return selected;
    }

    public void setSelected(Positionresponsibility selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PositionresponsibilityFacade getFacade() {
        return ejbFacade;
    }

    public Positionresponsibility prepareCreate() {
        selected = new Positionresponsibility();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setPositionResponsibilityPositionID(positionControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PositionresponsibilityCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PositionresponsibilityUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PositionresponsibilityDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Positionresponsibility> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Positionresponsibility> getPostionItems() {
        getItems();
        //Filter trainings
        if(positionControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getPositionResponsibilityPositionID().getPositionID(), positionControl.getSelected().getPositionID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    
    public Positionresponsibility getPositionresponsibility(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Positionresponsibility> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Positionresponsibility> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Positionresponsibility.class)
    public static class PositionresponsibilityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PositionresponsibilityController controller = (PositionresponsibilityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "positionresponsibilityController");
            return controller.getPositionresponsibility(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Positionresponsibility) {
                Positionresponsibility o = (Positionresponsibility) object;
                return getStringKey(o.getPositionResponsibilityID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Positionresponsibility.class.getName()});
                return null;
            }
        }

    }

}
