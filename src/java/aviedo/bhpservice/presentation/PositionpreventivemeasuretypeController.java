package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Positionpreventivemeasuretype;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.PositionpreventivemeasuretypeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("positionpreventivemeasuretypeController")
@SessionScoped
public class PositionpreventivemeasuretypeController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.PositionpreventivemeasuretypeFacade ejbFacade;
    private List<Positionpreventivemeasuretype> items = null;
    private Positionpreventivemeasuretype selected;

    public PositionpreventivemeasuretypeController() {
    }

    public Positionpreventivemeasuretype getSelected() {
        return selected;
    }

    public void setSelected(Positionpreventivemeasuretype selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PositionpreventivemeasuretypeFacade getFacade() {
        return ejbFacade;
    }

    public Positionpreventivemeasuretype prepareCreate() {
        selected = new Positionpreventivemeasuretype();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PositionpreventivemeasuretypeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PositionpreventivemeasuretypeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PositionpreventivemeasuretypeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Positionpreventivemeasuretype> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Positionpreventivemeasuretype getPositionpreventivemeasuretype(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Positionpreventivemeasuretype> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Positionpreventivemeasuretype> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Positionpreventivemeasuretype.class)
    public static class PositionpreventivemeasuretypeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PositionpreventivemeasuretypeController controller = (PositionpreventivemeasuretypeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "positionpreventivemeasuretypeController");
            return controller.getPositionpreventivemeasuretype(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Positionpreventivemeasuretype) {
                Positionpreventivemeasuretype o = (Positionpreventivemeasuretype) object;
                return getStringKey(o.getPositionPreventiveMeasureTypeID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Positionpreventivemeasuretype.class.getName()});
                return null;
            }
        }

    }

}
