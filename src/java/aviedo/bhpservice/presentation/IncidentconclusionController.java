package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Incidentconclusion;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.IncidentconclusionFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("incidentconclusionController")
@SessionScoped
public class IncidentconclusionController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.IncidentconclusionFacade ejbFacade;
    private List<Incidentconclusion> items = null;
    private Incidentconclusion selected;

    @Inject
    private IncidentController incidentControl;
    
    public IncidentconclusionController() {
    }

    public Incidentconclusion getSelected() {
        return selected;
    }

    public void setSelected(Incidentconclusion selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private IncidentconclusionFacade getFacade() {
        return ejbFacade;
    }

    public Incidentconclusion prepareCreate() {
        selected = new Incidentconclusion();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setIncidentConclusionIncidentID(incidentControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("IncidentconclusionCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("IncidentconclusionUpdated"));
    }

    public void destroy() {
        selected.setIncidentConclusionIncidentID(incidentControl.getSelected());
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("IncidentconclusionDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Incidentconclusion> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Incidentconclusion> getIncidentItems() {
        getItems();
        //Filter trainings
        if(incidentControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getIncidentConclusionIncidentID().getIncidentID(), incidentControl.getSelected().getIncidentID()))
                    .collect(Collectors.toList());
        
        }
        return null;
    }
    
    public Incidentconclusion getIncidentconclusion(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Incidentconclusion> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Incidentconclusion> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Incidentconclusion.class)
    public static class IncidentconclusionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            IncidentconclusionController controller = (IncidentconclusionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "incidentconclusionController");
            return controller.getIncidentconclusion(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Incidentconclusion) {
                Incidentconclusion o = (Incidentconclusion) object;
                return getStringKey(o.getIncidentConclusionID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Incidentconclusion.class.getName()});
                return null;
            }
        }

    }

}
