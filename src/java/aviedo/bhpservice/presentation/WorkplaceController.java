package aviedo.bhpservice.presentation;

import aviedo.bhpservice.entities.Workplace;
import aviedo.bhpservice.presentation.util.JsfUtil;
import aviedo.bhpservice.presentation.util.JsfUtil.PersistAction;
import aviedo.bhpservice.integration.WorkplaceFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@Named("workplaceController")
@SessionScoped
public class WorkplaceController implements Serializable {

    @EJB
    private aviedo.bhpservice.integration.WorkplaceFacade ejbFacade;
    private List<Workplace> items = null;
    private Workplace selected;

    
    @Inject
    private PositionController positionControl;
    
    public WorkplaceController() {
    }

    public Workplace getSelected() {
        return selected;
    }

    public void setSelected(Workplace selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private WorkplaceFacade getFacade() {
        return ejbFacade;
    }

    public Workplace prepareCreate() {
        selected = new Workplace();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setWorkplacePositionID(positionControl.getSelected());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("WorkplaceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("WorkplaceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("WorkplaceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Workplace> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }
    public List<Workplace> getPositionItems() {
        getItems();
        //Filter Ecucational activities
        if(positionControl.getSelected() != null){
            return items.stream().filter(a -> Objects
                    .equals(a.getWorkplacePositionID().getPositionID(), positionControl.getSelected().getPositionID()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public Workplace getWorkplace(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Workplace> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Workplace> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Workplace.class)
    public static class WorkplaceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            WorkplaceController controller = (WorkplaceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "workplaceController");
            return controller.getWorkplace(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Workplace) {
                Workplace o = (Workplace) object;
                return getStringKey(o.getWorkplaceID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Workplace.class.getName()});
                return null;
            }
        }

    }  

}
